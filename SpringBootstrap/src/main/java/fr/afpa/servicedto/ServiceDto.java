package fr.afpa.servicedto;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.entite.Personne;
import fr.afpa.entitedao.PersonneDao;
import fr.afpa.servicedao.ServiceDao;

public class ServiceDto {
	public boolean addUtilisateur(Personne personne) {
		ServiceDao serviceDao = new ServiceDao();

		PersonneDao personneDao = mappingPersonneMetierToPersonneDao(personne);
		
		return serviceDao.ajouterUtilisateur(personneDao);
	}
	
	public ArrayList<Personne> listUtilisateur(int start, int total) {
		ServiceDao serviceUtilisateurDao = new ServiceDao();
		ArrayList<PersonneDao> listeUtilisateurDao = serviceUtilisateurDao.afficherListeUtilisateur(start, total);
		ArrayList<Personne> listeUtilisateurMetier = null;
		if (listeUtilisateurDao != null) {
			listeUtilisateurMetier = (ArrayList<Personne>) mappingListPersonneDaoToListPersonneMetier(
					listeUtilisateurDao);
			return listeUtilisateurMetier;
		}
		return listeUtilisateurMetier;
	}
	
	public Personne mappingPersonneDaoToPersonneMetier(PersonneDao personneDao) {
		Personne personne = new Personne();

		personne.setIdPersonne(personneDao.getIdPersonne());
		personne.setNom(personneDao.getNom());
		personne.setPrenom(personneDao.getPrenom());
		personne.setEmail(personneDao.getEmail());
		personne.setTelephone(personneDao.getTelephone());
		personne.setBday(personneDao.getBday());
		return personne;
	}

	public PersonneDao mappingPersonneMetierToPersonneDao(Personne personne) {
		PersonneDao personneDao = new PersonneDao();
		personneDao.setIdPersonne(personne.getIdPersonne());
		personneDao.setNom(personne.getNom());
		personneDao.setPrenom(personne.getPrenom());
		personneDao.setEmail(personne.getEmail());
		personneDao.setTelephone(personne.getTelephone());
		personneDao.setBday(personne.getBday());
		return personneDao;

	}
	
	public ArrayList<PersonneDao> mappingListPersonneMetierToListPersonneDao(List<Personne> listPersonne) {
		ArrayList<PersonneDao> listPersonneDao = new ArrayList<PersonneDao>();

		for (Personne personne : listPersonne) {
			PersonneDao personneDao = mappingPersonneMetierToPersonneDao(personne);
			listPersonneDao.add(personneDao);
		}

		return listPersonneDao;
	}

	public List<Personne> mappingListPersonneDaoToListPersonneMetier(List<PersonneDao> listPersonneDao) {
		ArrayList<Personne> listPersonne = new ArrayList<Personne>();

		for (PersonneDao personneDao : listPersonneDao) {
			Personne personne = mappingPersonneDaoToPersonneMetier(personneDao);
			listPersonne.add(personne);
		}

		return listPersonne;
	}


}
