package fr.afpa.springbootstrap;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.entite.Personne;
import fr.afpa.servicedao.ServiceDao;
import fr.afpa.servicemetier.ServiceMetier;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	//redirige vers la page jsp home
	@RequestMapping(value ="/")
	public String home() {
		return "home";
	}

	
	//ajoute une personne � la base de donn�e
	@RequestMapping(value = "/add", method = RequestMethod.GET, params= {"nom", "prenom", "tel", "email", "bday"})
	public String addPerson(@RequestParam(value="nom") String nom, @RequestParam(value="prenom") String prenom, @RequestParam(value="tel") String tel, @RequestParam(value="email") String email, @RequestParam(value="bday") String bday) {
		ServiceMetier serviceMetier = new ServiceMetier();
		
		Personne p1 = new Personne();
		p1.setNom(nom);
		p1.setPrenom(prenom);
		p1.setTelephone(tel);
		p1.setEmail(email);
		p1.setBday(bday);
		
		serviceMetier.creationUtilisateur(p1);
			
		return "confirm";		
	}
	

	//r�cupere la liste des personnes et redirection vers la page jsp qui liste les personnes
	@RequestMapping(value ="/show")
	public ModelAndView showAll() {
		
		ServiceMetier sum = new ServiceMetier();
		ArrayList<Personne> listePersonne = new ArrayList<Personne>();
		int start = 1;
		int total = 5;
		int nombreTotalPersonne = new ServiceDao().getNombrePersonne();
		int nombreDePage = (nombreTotalPersonne / total)+1;
		listePersonne = sum.afficherListeUtilisateur(start, total);
	   	ModelAndView mv = new ModelAndView();
	   	mv.setViewName("liste");
	   	mv.addObject("nbre", nombreDePage);
	   	mv.addObject("list",listePersonne);
	   	return mv;  
	}
	
	@RequestMapping(value ="/page", method = RequestMethod.GET, params= {"page"})
	public ModelAndView showPage(@RequestParam(value="page") int page) {
		ServiceMetier sum = new ServiceMetier();
		ArrayList<Personne> listePersonne = new ArrayList<Personne>();
		int start = page;
		int total = 5;
		int nombreTotalPersonne = new ServiceDao().getNombrePersonne();
		int nombreDePage = (nombreTotalPersonne / total)+1;
		listePersonne = sum.afficherListeUtilisateur(start, total);
	   	ModelAndView mv = new ModelAndView();
	   	mv.setViewName("liste");
	   	mv.addObject("nbre", nombreDePage);
	   	mv.addObject("list",listePersonne);
	   	return mv;  
	}
}
