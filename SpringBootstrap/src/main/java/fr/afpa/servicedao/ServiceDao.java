package fr.afpa.servicedao;

import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import fr.afpa.entitedao.PersonneDao;
import fr.afpa.utils.HibernateUtils;

public class ServiceDao {
	//ajouter une personne
	public boolean ajouterUtilisateur(PersonneDao personne) {

		boolean retour = false;
		Session session = null;
		Transaction transaction = null;
		try {
			session = HibernateUtils.getSession();
			transaction = session.beginTransaction();

			session.save(personne);
			transaction.commit();
			retour = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();

		}
		return retour;
	}
	
	//recuperer la liste de personne
	public ArrayList<PersonneDao> afficherListeUtilisateur(int start, int total) {
		Session session = null;
		Transaction transaction = null;
		ArrayList<PersonneDao> listeUtilisateur = null;

		try {
			session = HibernateUtils.getSession();
			transaction = session.beginTransaction();
			 Query<PersonneDao> query = session.createQuery("from PersonneDao", PersonneDao.class);
			 query.setFirstResult((start - 1) * total);
		     query.setMaxResults(total);
			listeUtilisateur = (ArrayList<PersonneDao>) query.list();
			transaction.commit();

		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			session.close();
		}
		return listeUtilisateur;
	}
	
	//r�cup�rer le nombre de personne
	public int getNombrePersonne() {
		Session session = null;
		Transaction transaction = null;
		int nombre = 0;

		try {
			session = HibernateUtils.getSession();
			transaction = session.beginTransaction();
			nombre = ((Long) session.createQuery("select count(*) from PersonneDao").uniqueResult()).intValue();
			transaction.commit();

		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			session.close();
		}
		return nombre;
	}
}
