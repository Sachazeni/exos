<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Home</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css">
  
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Pr�sentation</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Accueil <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="qui">Qui sommes nous</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown">
          Menu
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Menu du pauvre</a>
          <a class="dropdown-item" href="#">Menu du riche</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Menu � volont�</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#">Prout</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Rechercher</button>
    </form>
  </div>
</nav>
<div class="container-fluid bg">
<div class="row justify-content-center">
<div class="container-fluid col-sm-5">
	<h2>Cr�er un utilisateur !</h2>
	<form action="add" class="form-container">
	 
	  <div class="form-group">
	    <label for="nom">Nom : </label>
	    <input type="text" class="form-control" name="nom" id="nom" placeholder="Entrer votre nom" required>
	  </div>
	  <div class="form-group">
	    <label for="prenom">Prenom :</label>
	    <input type="text" class="form-control" name="prenom" id="prenom" placeholder="Entrer votre prenom" required>
	  </div>
	  <div class="form-group">
	    <label for="tel">Tel :</label>
	    <input type="text" class="form-control" name="tel" id="tel" placeholder="Entrer votre telephone" required>
	  </div>
	  <div class="form-group">
	    <label for="email">Email :</label>
	    <input type="email" class="form-control" name="email" id="email" placeholder="Entrer votre email" required>
	  </div>
	  <div class="form-group" id="picker">
	  	 <label for="dateNaissance">Date de naissance :</label>
	    <input class="datepicker" data-date-format="mm/dd/yyyy" name="bday" id="bday">
	  </div>
	 
	  
	  <button type="submit" name="create" class="btn btn-primary">Valider</button>
	</form>
	 
	</div>
	</div>
	</div>
	<script src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
	<script>
	$('.datepicker').datepicker({
	    startDate: '0d'
	});
	</script>
</body>
</html>
