<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>

<!DOCTYPE html>
<html>
<head>

<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Pr�sentation</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Accueil <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Qui sommes nous</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown">
          Menu
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Menu du pauvre</a>
          <a class="dropdown-item" href="#">Menu du riche</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Menu � volont�</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#">Prout</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Rechercher</button>
    </form>
  </div>
</nav>
<div class="container-fluid col-sm-12">
	<p>Voici la liste de tous les utilisateurs</p>
	<table class="table table-striped" >
  <thead>
    <tr>
      <th>Id</th>
      <th>Nom</th>
      <th>Prenom</th>
      <th>Tel</th>
      <th>Email</th>
      <th>Date de naissance</th>
    </tr>
  </thead>
  <tbody>
	<c:forEach items="${ list }" var="element">
	  		 <tr>
		      <td><c:out value="${ element.idPersonne }"></c:out></td>
		      <td><c:out value="${ element.nom }"></c:out></td>
		      <td><c:out value="${ element.prenom }"></c:out></td>
		      <td><c:out value="${ element.telephone }"></c:out></td>
		      <td><c:out value="${ element.email }"></c:out></td>
		      <td><c:out value="${ element.bday }"></c:out></td>
		   	 </tr>
	  	</c:forEach>
  </tbody>
</table>
<nav>
	<ul class="pagination">
		<c:forEach var="i" begin="1" end="${ nbre }" step="1">
					
			<li class="page-item"><a class="page-link" href="page?page=${ i }">${ i }</a></li>	    
					
		</c:forEach>
	</ul>
</nav>
</div>
</body>
<script src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
</html>