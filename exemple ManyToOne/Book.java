package com.demo.jpa;

import java.util.ArrayList;
import java.util.List;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name="Livre")
public class Book {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="book_id")
	private int idBook;
	@Column(name="auteur")
	private String auteur;
	@Column(name="titre")
	private String titre;
	@Column(name="prix")
	private Integer prix;
	@Column(name="isbn")
	private String isbn;
	
	@OneToMany(mappedBy="book")
	private List<Page> pageList;
	
    
	
	/**
	 * 
	 * @param idBook
	 * @param auteur
	 * @param titre
	 * @param prix
	 * @param isbn
	 */
	public Book(int idBook, String auteur, String titre, Integer prix,String isbn) {

		this.idBook = idBook;
		this.auteur = auteur;
		this.titre = titre;
		this.prix = prix;
		this.isbn = isbn;
		pageList = new ArrayList<>();
	}
	
	public Book() {
		
		this.auteur = "";
		this.titre = "";
		this.prix =0;
		this.isbn = "";
	}
	
	/**
	 * 
	 * @param auteur
	 * @param titre
	 * @param prix
	 * @param isbn
	 */
	public Book(String auteur, String titre,  String isbn, Integer prix) {
		
		this.auteur = auteur;
		this.titre = titre;
		this.prix = prix;
		this.isbn = isbn;
		pageList = new ArrayList<>();
		
	}


	/**
	 * @return the idBook
	 */
	public int getIdBook() {
		return idBook;
	}
	/**
	 * @param idBook the idBook to set
	 */
	public void setIdBook(int idBook) {
		this.idBook = idBook;
	}
	/**
	 * @return the auteur
	 */
	public String getAuteur() {
		return auteur;
	}
	/**
	 * @param auteur the auteur to set
	 */
	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}
	/**
	 * @return the titre
	 */
	public String getTitre() {
		return titre;
	}
	/**
	 * @param titre the titre to set
	 */
	public void setTitre(String titre) {
		this.titre = titre;
	}
	/**
	 * @return the prix
	 */
	public Integer getPrix() {
		return prix;
	}
	/**
	 * @param prix the prix to set
	 */
	public void setPrix(Integer prix) {
		this.prix = prix;
	}
	/**
	 * @return the isbn
	 */
	public String getIsbn() {
		return isbn;
	}
	/**
	 * @param isbn the isbn to set
	 */
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	/*@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((auteur == null) ? 0 : auteur.hashCode());
		result = prime * result + idBook;
		result = prime * result + ((isbn == null) ? 0 : isbn.hashCode());
		result = prime * result + ((prix == null) ? 0 : prix.hashCode());
		result = prime * result + ((titre == null) ? 0 : titre.hashCode());
		return result;
	}
	 (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (auteur == null) {
			if (other.auteur != null)
				return false;
		} else if (!auteur.equals(other.auteur))
			return false;
		if (idBook != other.idBook)
			return false;
		if (isbn == null) {
			if (other.isbn != null)
				return false;
		} else if (!isbn.equals(other.isbn))
			return false;
		if (prix == null) {
			if (other.prix != null)
				return false;
		} else if (!prix.equals(other.prix))
			return false;
		if (titre == null) {
			if (other.titre != null)
				return false;
		} else if (!titre.equals(other.titre))
			return false;
		return true;
	}*/
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Book [idBook=" + idBook + ", auteur=" + auteur + ", titre="
				+ titre + ", prix=" + prix + ", isbn=" + isbn + "]";
	}

	/**
	 * @return the pageList
	 */
	public List<Page> getPageList() {
		return pageList;
	}

	/**
	 * @param pageList the pageList to set
	 */
	public void setPageList(List<Page> pageList) {
		this.pageList = pageList;
	}

	
	

	
	
}
