package com.jpa.main;


import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.demo.jpa.Book;
import com.demo.jpa.Page;
import fr.cortex2i.utils.HibernateUtils;


public class Main {
	private static Session s = null;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// Ouverture d'une session Hibernate
		s = HibernateUtils.getSession();
				
		
		
		// Instanciation d'objet
		Book monPremierLivre = new Book();
		
		monPremierLivre.setAuteur("BENJIRA");
		monPremierLivre.setIsbn("87545678");
		monPremierLivre.setPrix(20);
		monPremierLivre.setTitre("CDA JAVA");
		
		// Instanciation de la liste de page
		List<Page> pageList = new ArrayList<Page>();
		
		Page p = new Page("contenu", 5);
		Page p1 = new Page("contenu1", 10);	
		
		// Ajout des objets � notre liste 
		pageList.add(p);
		pageList.add(p1);
		
		p.setBook(monPremierLivre);
		p1.setBook(monPremierLivre);
				
		
		monPremierLivre.setPageList(pageList);
		//monPremierLivre.getPageList().add(new Page("MIAW", 15));
		
		
		// D�but de la transaction
		Transaction tx = s.beginTransaction();
		// Enregistrements de l'entit� m�re
		s.save(monPremierLivre);
		
		// Enregistrements des entit�s filles
		s.save(p);
		s.save(p1);
     
		tx.commit();

		// Lister les livres
		listerLivres();
		// Fermeture de la session Hibernate
		s.close();
				
	}
	
	
	// Affiche le contenu de la table USERS
		@SuppressWarnings("unchecked")
		private static void listerLivres() {
			System.out.println("---Book---");
			// Cr�ation de la requ�te
			Query q = s.createQuery("from Book");
			
			ArrayList<Book> list = (ArrayList<Book>) q.list();
			// Affichage de la liste de r�sultats
			for (Book livre: list) {
						System.out.println("Book ==> [idBook] =" + livre.getIdBook()  + "\t" +
						"[auteur] = " + livre.getAuteur()  + "\t" +
						"[titre ] = "  + livre.getTitre()  + "\t" +
						"[prix ] = " + livre.getPrix()  + "\t" +
						"[isbn ] = " + livre.getIsbn() + "]" );
						
						if(livre.getPageList()!=null && !livre.getPageList().isEmpty()) {
							System.out.println("-------------  Liste de pages du livre   ------------------");
							for (Page page : livre.getPageList()) {
								
								System.out.println("Page ==> [IdPage] =" + page.getIdPage()  + "\t" +
										"[Num�ro de la  page] = " + page.getNumPage()  + "\t" +
										"[Contenu de la page ] = "  + page.getContenu()  + "\t" );
								
							}
						}
						else {
							System.out.println("Ce livre ne contient aucune page !");
						}
			}
		}

}
