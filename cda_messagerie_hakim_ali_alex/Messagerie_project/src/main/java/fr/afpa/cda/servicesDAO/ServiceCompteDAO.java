package fr.afpa.cda.servicesDAO;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.query.Query;

import fr.afpa.cda.entiteDAO.CompteDAO;
import fr.afpa.cda.utils.HibernateUtils;



public class ServiceCompteDAO {

	/**
	 * demande à la bdd si le login et le mot de passe sont corrects, si oui renvoie de l'authentificationDaO correspondant
	 * @param authDaoVerif : l'authentification à vérifier
	 * @return : une entité authentificationDAO si le login et le mot de passe sont corrects, null sinon
	 */
	public CompteDAO authentification(CompteDAO authDaoVerif) {
		Session s = null;
		CompteDAO authDaoResultat = null;
		
		try {
			s = HibernateUtils.getSession();
			Query q = s.getNamedQuery("verifAuth");
			q.setParameter("login", authDaoVerif.getLogin());
			q.setParameter("mdp", authDaoVerif.getMotDePasse());
			authDaoResultat = (CompteDAO) q.getSingleResult();
			
		}catch (Exception e) {
		Logger.getLogger(ServiceCompteDAO.class.getName()).log(Level.SEVERE, null, e);
			
		}finally {
			if(s!=null) {
				try {
					s.close();
				} catch (Exception e) {
					Logger.getLogger(ServiceCompteDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			}
		}
		return authDaoResultat;
	}
}
