package fr.afpa.cda.entiteMetier;

import java.time.LocalDate;
import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@ToString(of= {"idPersonne","nom","prenom","actif", "dateDeNaissance","compte","messagesEnv","messagesRecus"})
public class Personne {

	 int idPersonne;
	 String nom;
	 String prenom;
	 LocalDate dateDeNaissance;
	 
	 // le boolean admin n'est pas disponible dans les vues, juste pour créer un seul super admin si besoin
	 Boolean admin;
	 //le boolean si la personne est activé ou non
	 Boolean actif;
	 
	 Compte compte;
	 List<Message>messagesEnv;
	 List<Message>messagesRecus;
	
}
