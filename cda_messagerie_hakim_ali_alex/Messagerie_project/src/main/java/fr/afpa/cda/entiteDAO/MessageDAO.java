package fr.afpa.cda.entiteDAO;


import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.NamedQuery;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;


@Getter
@Setter
@ToString(of= {"idMessage","sujet","contenu", "idDestinataire","idExpediteur","archiveDestinataire","archiveExpediteur"})
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name="message")
@NamedQuery(name = "rechercheEnvoyes",query = "from MessageDAO where idExpediteur = :idExpediteur")
@NamedQuery(name = "rechercheRecus",query = "from MessageDAO where idDestinataire = :idDestinataire")
public class MessageDAO {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column (name="idMessage", updatable = false, nullable = false )
	int idMessage;
	
	
	@Column(name = "dateEnvoi", updatable =false, nullable=false)
	LocalDateTime dateEnvoi;
	
	@Column
	String sujet;
	
	
	@Column
	String contenu;
	
	@ManyToOne(cascade = CascadeType.ALL )
	@JoinColumn(name="id_destinataire")
	PersonneDAO idDestinataire;
	
	@ManyToOne(cascade = CascadeType.ALL )
	@JoinColumn(name="id_expediteur")
	PersonneDAO idExpediteur; 
	
	@Column
	boolean archiveDestinataire;
	
	@Column
	boolean archiveExpediteur;
	
	
	
}
