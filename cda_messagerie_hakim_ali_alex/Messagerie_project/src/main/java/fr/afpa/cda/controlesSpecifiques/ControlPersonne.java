package fr.afpa.cda.controlesSpecifiques;

import fr.afpa.cda.entiteMetier.Personne;
import fr.afpa.cda.servicesMetier.ServiceRecherche;

public class ControlPersonne {
	
	public static boolean loginDisponible(String login) {
		Personne pers = new ServiceRecherche().rechercheUnePersonne(login);
		boolean flag =false;
		if(pers==null) {
			flag=true;
		}
		return flag;
		
	}

}
