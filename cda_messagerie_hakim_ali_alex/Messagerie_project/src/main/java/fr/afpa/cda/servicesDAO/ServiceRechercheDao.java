package fr.afpa.cda.servicesDAO;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.query.Query;

import fr.afpa.cda.entiteDAO.PersonneDAO;
import fr.afpa.cda.utils.HibernateUtils;

public class ServiceRechercheDao {

	/**
	 * Service DAO qui execute la requete de recherche personneDAO via le login ou
	 * le status (actif ou inactif de la personne)
	 * 
	 * @param typeRecherche
	 * @param valeur        : login si typeRecherche=login, true ou false si type de
	 *                      recherche=actif
	 * 
	 * @return : une liste d'entités PersonnesDao récupéré via la requete
	 */
	public List<PersonneDAO> rechercheByLoginOrActif(String typeRecherche, String valeur) {
		Session s = null;
		List<PersonneDAO> listePersDao = null;

		try {
			s = HibernateUtils.getSession();
			Query q = null;

			if ("login".equals(typeRecherche)) {
				q = s.getNamedQuery("rechercheByLogin");
				q.setParameter("login", valeur);
			} else if ("actif".equals(typeRecherche)) {
				q = s.getNamedQuery("rechercheDesPersStatus");

				if ("true".equals(valeur)) {
					q.setParameter("actif", true);
				} else if ("false".equals(valeur)) {
					q.setParameter("actif", false);
				}

			}
			if (q != null) {
				listePersDao = q.list();
			}

		} catch (Exception e) {
			Logger.getLogger(ServiceRechercheDao.class.getName()).log(Level.SEVERE, null, e);

		} finally {
			if (s != null) {
				try {
					s.close();
				} catch (Exception e) {
					Logger.getLogger(ServiceRechercheDao.class.getName()).log(Level.SEVERE, null, e);
				}
			}
		}
		return listePersDao;
	}

	/**
	 * Service dao qui execute la requete pour avoir toutes les personneDAO
	 * 
	 * @return : une liste d'entités PersonnesDao récupéré via la requete
	 */
	public List<PersonneDAO> getAllPersonnes() {
		Session s = null;
		List<PersonneDAO> listePersDao = null;

		try {
			s = HibernateUtils.getSession();
			Query q = null;
			q = s.getNamedQuery("getAll");
			if (q != null) {
				listePersDao = q.list();
			}

		} catch (Exception e) {
			Logger.getLogger(ServiceRechercheDao.class.getName()).log(Level.SEVERE, null, e);

		} finally {
			if (s != null) {
				try {
					s.close();
				} catch (Exception e) {
					Logger.getLogger(ServiceRechercheDao.class.getName()).log(Level.SEVERE, null, e);
				}
			}
		}
		return listePersDao;
	}

	/**
	 * Methode pour recuperer une seule personne via son login (pour eviter de parcourir
	 * une liste
	 * @param login : une chaine de caractere qui correspond au login d'une personne
	 * @return la personneDAO trouvé;
	 */
	public PersonneDAO recupUneSeulePersonne(String login) {
		Session s = null;
		PersonneDAO persDao = null;
		
		try {
			s = HibernateUtils.getSession();
			Query q = null;
			q = s.getNamedQuery("rechercheByLogin");
			q.setParameter("login", login);

			if (q != null) {

				persDao = (PersonneDAO) q.getSingleResult();
			}
		} catch (Exception e) {

		} finally {
			if (s != null) {
				try {
					s.close();
				} catch (Exception e) {

				}
			}
			
		}
		return persDao;
	}
	
	

}
