package fr.afpa.cda.serviceDTO;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.cda.entiteDAO.MessageDAO;
import fr.afpa.cda.entiteDAO.PersonneDAO;
import fr.afpa.cda.entiteMetier.Message;
import fr.afpa.cda.servicesDAO.ServiceMessageDAO;

public class ServiceMessageDTO {

	/**
	 * Methode pour transformer un message metier vers une entite persistante dans la base de données
	 * 
	 * @param messageMetier : le message à transformer (pour l'envoye du message)
	 * @return msgDao: le message transformé en une entité persistante
	 */
	public static MessageDAO messageMetierVersMessDao(Message messageMetier) {
		MessageDAO msgDao = new MessageDAO();

		// set tous les attributs
		msgDao.setIdMessage(messageMetier.getIdMessage());
		msgDao.setSujet(messageMetier.getSujet());
		msgDao.setContenu(messageMetier.getContenu());
		msgDao.setArchiveExpediteur(messageMetier.isArchiveExpediteur());
		msgDao.setArchiveDestinataire(messageMetier.isArchiveDestinataire());
		
		msgDao.setIdExpediteur(ServicesPersonneDTO.personneMetierToPersonneDao(messageMetier.getPersonneExpediteur()));
		
		msgDao.setIdDestinataire(
				ServicesPersonneDTO.personneMetierToPersonneDao(messageMetier.getPersonneDestinataire()));

		return msgDao;
	}
	
	public static MessageDAO archiveMetierVersarchiveDao(Message messageMetier) {
		MessageDAO msgDao = new MessageDAO();

		msgDao.setArchiveDestinataire(messageMetier.isArchiveDestinataire());

		return msgDao;
	}
	

	/**
	 * Methode pour transformer des messages persistants en messages metiers
	 * 
	 * @param msgDao : message recupérée dans la base de données
	 * @return msgMetier: le message transformé en message metier
	 */
	public Message messageDaoVersMessMetier(MessageDAO messageDAO) {
		
		Message msgMetier = new Message();

		msgMetier.setIdMessage(messageDAO.getIdMessage());
		msgMetier.setSujet(messageDAO.getSujet());
		msgMetier.setContenu(messageDAO.getContenu());
		msgMetier.setArchiveExpediteur(messageDAO.isArchiveExpediteur());
		msgMetier.setArchiveDestinataire(messageDAO.isArchiveDestinataire());
		msgMetier.setPersonneExpediteur(ServicesPersonneDTO.personneDaoToPersonneMetier(messageDAO.getIdExpediteur()));
		msgMetier.setPersonneDestinataire(ServicesPersonneDTO.personneDaoToPersonneMetier(messageDAO.getIdDestinataire()));

	return msgMetier;

}
	/**
	 * Transformation d'une liste de messages DAO vers une liste de messagesMetier
	 * @param listeMessagesDao : liste des messages recuperée
	 * @return listeMetier :
	 */
	public List<Message>listeMessagesDaoVersListeMetier(List<MessageDAO>listeMessagesDao){
		List<Message>listeMessagesMetier=new ArrayList<Message>();
		
		for (MessageDAO msgDao : listeMessagesDao) {
			listeMessagesMetier.add(messageDaoVersMessMetier(msgDao));	
		}
	return listeMessagesMetier;
	}

	
	
	
	
	
	/**
	 * Création d'un message reliant la methode DTO avec le DAO
	 * @param message
	 * @return true si l'envoi à a abouti
	 */
	public boolean envoyerMessage(Message message) {
		MessageDAO msgDao = messageMetierVersMessDao(message);
		return new ServiceMessageDAO().saveMessage(msgDao);

	}
	
	public boolean archiverMessage(int idMessage) {
		ServiceMessageDAO serviceMessageDAO = new ServiceMessageDAO();
		return serviceMessageDAO.archiverMessage(idMessage);
	}
	
}
