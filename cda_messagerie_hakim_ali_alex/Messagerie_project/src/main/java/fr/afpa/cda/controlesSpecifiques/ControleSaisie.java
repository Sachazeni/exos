package fr.afpa.cda.controlesSpecifiques;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


public class ControleSaisie {
	/**
	 * methode qui verifie si la chaine de caractere et non vide (les espaces sont concidéré comme vide)
	 * @param chaine chaine de caractère à vérifier
	 * @return true si la chaine de caractère est non vide et n'est pas constitué que des espaces, false sinon
	 */
	public static boolean isNonVide(String chaine) {
		return  chaine.replaceAll(" ", "").length()>0;
			
	}
	
	/**
	 * Comtrole si le nom ou le prenom est valide
	 * 
	 * @param nom : le nom/prenom a controler
	 * @return true si le nom est valide
	 */
	public static boolean isNom(String nom) {
		return nom.matches("^[A-Za-z][A-Za-z- ]*$");
	}
	
	/**
	 * controle si la date de naissance est valide
	 * 
	 * @param date date à controller
	 * @return true si la chaine est une date valide, false sinon
	 */
	public static boolean isDateValide(String date) {

		try {
			LocalDate date2 = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd"));

				return true;
			
		} catch (Exception e) {
			
			return false;
		}
	

	}
}
