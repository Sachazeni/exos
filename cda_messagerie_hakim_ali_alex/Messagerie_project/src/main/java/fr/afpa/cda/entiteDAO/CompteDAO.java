package fr.afpa.cda.entiteDAO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;


import org.hibernate.annotations.NamedQuery;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@ToString(of= {"login","motDePasse"})

@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name="compte")
@NamedQuery(name = "verifCompte",query = "from CompteDAO where login = :login and mdp = : mdp")
public class CompteDAO {


	@Id
	@Column (name="login", updatable = false, nullable = false, length = 50 )
	String login;

	@Column (name="motDePasse", updatable = true, nullable = false,length = 50)
	String motDePasse;
	
	@OneToOne(mappedBy = "compte")
	PersonneDAO personne;
	
}
