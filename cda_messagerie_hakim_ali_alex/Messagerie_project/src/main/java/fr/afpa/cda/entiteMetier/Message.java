package fr.afpa.cda.entiteMetier;

import java.time.LocalDateTime;


import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(of = {"idMessage","sujet","contenu","date","personneExpediteur","personneDestinataire"})
public class Message {

	int idMessage;
	String sujet;
	String contenu;
	LocalDateTime date;
	Personne personneExpediteur;
	Personne personneDestinataire;
	boolean archiveDestinataire;
	boolean archiveExpediteur;


}
