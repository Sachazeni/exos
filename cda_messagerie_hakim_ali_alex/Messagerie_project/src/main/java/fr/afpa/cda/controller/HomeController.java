package fr.afpa.cda.controller;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import fr.afpa.cda.entiteMetier.Compte;
import fr.afpa.cda.entiteMetier.Personne;
import fr.afpa.cda.serviceDTO.ServicesPersonneDTO;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/")
	public String connexion() {
		return "connexion";
	}

	@RequestMapping(value = "/creationCompte")
	public String nouveauClient(Model model) {

		return "creationCompte";
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET, params = { "login", "password", "nom", "prenom",
			"dateNaissance" })
	public String ajouterUtilisateur(@RequestParam(value = "login") String login, @RequestParam(value = "password") String password,
			@RequestParam(value = "nom") String nom, @RequestParam(value = "prenom") String prenom,
			@RequestParam(value = "dateNaissance") LocalDate dateNaissance) {

		Compte compte = new Compte();
		compte.setLogin(login);
		compte.setMotDePasse(password);
		Personne personne = new Personne();
		personne.setNom(nom);
		personne.setPrenom(prenom);
		personne.setDateDeNaissance(dateNaissance);
		personne.setCompte(compte);

		new ServicesPersonneDTO().savePersonne(personne);

		return "validationCreationCompte";
	}

}
