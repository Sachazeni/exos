import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

public class QrCode {
	public static void main(String[] args) throws WriterException, IOException {
		String motACoder = "bonjour";

		BitMatrix motCoder = new QRCodeWriter().encode(motACoder, BarcodeFormat.QR_CODE, 200, 200);

		// Sauvegarder dans un fichier
		String format = "png"; // format de sortie du fichier
		String cheminDuFichier = "C:\\ENV\\workspace\\QrCode\\image"; // chemin de sortie du fichier
		String nomDuFichier = "QRCode"; // nom du fichier de sortie

		// sortie du fichier
		// creation du flux de sortie pour pouvoir ecrire
		FileOutputStream fluxDeSortie = new FileOutputStream(
				new File(cheminDuFichier + "\\" + nomDuFichier + "." + format));
		// ecriture sur le flux de sortie
		MatrixToImageWriter.writeToStream(motCoder, format, fluxDeSortie);
		// fermeture du flux
		fluxDeSortie.close();
	}
}
