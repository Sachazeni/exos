create or replace procedure ins_empo( ) 
language plpgsql as $$
begin
insert into emp values ('465564', 'ZIMMER', 'MARINE', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
return 2;
end $$;
call ins_empo();


create or replace procedure ins_emp3(id integer) 
language plpgsql as $$
begin
insert into emp values (id, 'ZIMMER', 'MARINE', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
end $$;
call ins_emp3(32121);
execute procedure sel_emp()

execute ins_emp3(44654);


create or replace procedure ins_emp5(id integer) as
$BODY$
begin
insert into emp values (id, 'ZIMMER', 'MARINE', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
end 
$BODY$
language plpgsql;

create or replace procedure ins_emp5(id integer,id2 integer) as
$BODY$
begin
insert into emp values (id, 'ZIMMER', 'MARINE', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
insert into emp values (id2, 'ZIMMER', 'MARINE', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
end 
$BODY$
language plpgsql;

call ins_emp5(3215271,6565);










-------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------

create or replace procedure test(id integer) as
$BODY$
declare
idClient integer :=id;
c cursor for select * from emp e;
test record;
begin
	open c;
	fetch c into test;
insert into emp values ('154331544', test.nom, test.prenom, 'PROGRAMMEUR','1500', now(), 40000, 5000,5);

end 
$BODY$
language plpgsql;

call test(50);
select * from emp e;




-----------------------------------------------------------------------------------------
-------- insertion emp param num id debut (id fin 55)
---------------------------------------------------------------------------------------------
create or replace procedure ins_emp_for(id integer) as
$BODY$
declare
idClient integer :=id;
begin
	for i in idclient..55 loop
insert into emp values (i, 'SZIMMER', 'MARINE', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
end loop;
end 
$BODY$
language plpgsql;

call ins_emp_for(50);
select * from emp e;


-----------------------------------------------------------------------------------------
-------- insertion emp param  iddebut idfin (si idfin<iddep reverse
---------------------------------------------------------------------------------------------
create or replace procedure ins_emp_for(idDebut integer, idFin integer) as
$BODY$
declare
nom emp.nom%type;
begin
	nom:='TOTO';
if idDebut<idFin THEN
	for i in idDebut..idFin loop
	insert into emp values (i, nom, 'TITI', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
	end loop;
else
for i in reverse idDebut..idFin loop
	insert into emp values (i, 'TITI', nom, 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
	end loop;
end if;
end 
$BODY$
language plpgsql;

call ins_emp_for(230,225);
select * from emp e;

-----------------------------------------------------------------------------------------------------
----------------------------utilisation cursor
------------------------------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE public.ins_emp_cursor(iddebut integer, idfin integer)
 LANGUAGE plpgsql
AS $procedure$
declare
c cursor for select count(*) from emp e;
nom integer;
begin
	open c;
	fetch    c into nom;
insert into emp values (nom, 'nomPrenom.nom', 'nomPrenom.prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
close c;
end 
$procedure$
;
-----------------------------------------------------------------------------------------------------
----------------------------utilisation cursor
------------------------------------------------------------------------------------------------------

create or replace procedure test(id integer) as
$BODY$
declare
idClient integer :=id;
c cursor for select * from emp e;
test record;
begin
	open c;
	fetch c into test;
insert into emp values ('154331544', test.nom, test.prenom, 'PROGRAMMEUR','1500', now(), 40000, 5000,5);

end 
$BODY$
language plpgsql;

call test(50);
select * from emp e;
-----------------------------------------------------------------------------------------------------
----------------------------utilisation cursor while curseur%found loopend loop;
------------------------------------------------------------------------------------------------------
create or replace procedure emp_cursor_inv() as
$BODY$
declare
curseur cursor for select nom, prenom, noemp from emp e;
ligne record;
indice integer =500;
begin
	open curseur;
fetch curseur into ligne;
	
		update  emp set nom=ligne.prenom, prenom=ligne.nom where noemp=ligne.noemp;
		indice:=indice+1;
		fetch curseur into ligne;
	update  emp set nom=ligne.prenom, prenom=ligne.nom where noemp=ligne.noemp;
close curseur;
end 
$BODY$
language plpgsql;


call emp_cursor_inv();
select * from emp;
----------------------------------------------------------------------------------------
create or replace procedure ins_emp_cursor(idDebut integer, idFin integer) as
$BODY$
declare
c cursor for select count(*) from emp e group by emploi;
nom integer;
prenom c%rowtype;
begin
	open c;
	fetch    c into nom;
while c%found loop
insert into emp values (nom, 'nomPrenom.nom', 'nomPrenom.prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
fetch    c into nom;
end loop;
close c;
end 
$BODY$
language plpgsql;



call ins_emp_cursor(123,564);
select count(* )from emp e;





-----------------------------------------------------------------------------------------------------
----------------------------utilisation cursor while curseur%found loopend loop;
------------------------------------------------------------------------------------------------------
create or replace procedure emp_cursor_inv2() as
$BODY$
declare
curseur cursor for select nom, prenom, noemp from emp e;
ligne record;
begin
	open curseur;
loop
	fetch curseur into ligne;
	exit when not found;
	update  emp set nom=ligne.prenom, prenom=ligne.nom where noemp=ligne.noemp;
	RAISE notice 'I got here:% is the new value', ligne.nom;
end loop;
	close curseur;
end 
$BODY$
language plpgsql;


call emp_cursor_inv2();
select * from emp;



-----------------------------------------------------------------------------------------------------
----------------------------utilisation cursor while curseur%found loopend loop;
------------------------------------------------------------------------------------------------------
create or replace procedure emp_cursor_inv3() as
$BODY$
declare
curSeur cursor for select * from emp e;
ligne emp%ROWTYPE;
begin
	open curseur;
loop
	fetch curseur into ligne;
	exit when not found;
	update  emp set nom=ligne.prenom, prenom=ligne.nom where noemp=ligne.noemp;
end loop;
	close curseur;
end 
$BODY$
language plpgsql;


call emp_cursor_inv3();
select * from emp;

-----------------------------------------------------------------------------------------------------
----------------------------utilisation cursor exeptionjacques exception;
------------------------------------------------------------------------------------------------------
create or replace procedure emp_exception() as
$BODY$
declare
curseur cursor for select nom, prenom, noemp from emp e;
ligne record;
indice integer :=500;

begin
	open curseur;
loop
	fetch curseur into ligne;
	exit when not found;
	if(ligne.prenom='JACQUES') then
	raise exception 'JACQUES';
end if;
	update  emp set nom=ligne.prenom, prenom=ligne.nom where noemp=ligne.noemp;
	
end loop;
	close curseur;


end 
$BODY$
language plpgsql;


call emp_exception();
select * from emp e;


	-------------------------------------------------------
		-------------------------------------------------------
		-------------------------------------------------------
create or replace procedure count_emp() 

language plpgsql as
$BODY$
DECLARE 
nb_emp integer;
BEGIN
SELECT count(*) INTO nb_emp FROM emp e group by e.emploi order  by 1 desc;
insert into emp values (4, 'atest.nom', 'nomPrenom.prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
rollback;
if(found) then
insert into emp values (5, 'btest.nom', 'nomPrenom.prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
end if;
END 
$BODY$;

call count_emp();


---------------------------------------------
-----------EXCEPTION
----------------------------------
 y := x / 0;
  EXCEPTION
  WHEN division_by_zero THEN 
  
 create or replace procedure eception(id integer) 
language plpgsql as
$BODY$
DECLARE 
x integer :=5;
y integer;
begin
--y := x / 0;

insert into emp values (id, 'execption', 'nomPrenom.prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
  EXCEPTION
  WHEN division_by_zero THEN 
 insert into emp values (10, 'execption', 'nomPrenom.prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
  WHEN unique_violation THEN 
 insert into emp values (13, 'execption', 'nomPrenom.prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);

END 
$BODY$;
  

call eception(10);
select * from emp e;
  

 create or replace procedure eception2(id integer) 
language plpgsql as
$BODY$
DECLARE 
x integer :=5;
y integer;
begin
	
	raise notice 'toto' ;
  EXCEPTION
  WHEN division_by_zero THEN 
 insert into emp values (10, 'execption', 'nomPrenom.prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
  WHEN unique_violation THEN 
 insert into emp values (13, 'execption', 'nomPrenom.prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
when sqlstate 'P0001' then
insert into emp values (id, 'totoexecption', 'nomPrenom.prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);

END 
$BODY$;
  

call eception(2);
select * from emp e;

 create or replace procedure eception(id integer) 
language plpgsql as
$BODY$
DECLARE 
x integer :=5;
y integer;
BEGIN
INSERT INTO emp VALUES (1102, 'execption', 'nomPrenom.prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
 EXCEPTION
 WHEN ot THEN 
END 
$BODY$;


create or replace procedure multi_insertion(id integer) 
language plpgsql as
$BODY$
DECLARE 
BEGIN
INSERT INTO emp VALUES (id, 'nom1', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
INSERT INTO emp VALUES (id+1, 'nom2', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
INSERT INTO emp VALUES (id+2, 'nom3', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
INSERT INTO emp VALUES (1102, 'nom4', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
INSERT INTO emp VALUES (id+3, 'nom5', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
END 
$BODY$;

----------------------------------
-----------transaction
--------------------------------------
delete from emp where noemp<10;
select * from emp;
create or replace procedure multi_insertion() 
language plpgsql as
$BODY$
DECLARE 
BEGIN
INSERT INTO emp VALUES (1, 'nom1', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
INSERT INTO emp VALUES (2, 'nom2', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
INSERT INTO emp VALUES (3, 'nom3', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
INSERT INTO emp VALUES (1102, 'nom1102', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
INSERT INTO emp VALUES (4, 'nom5', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
END 
$BODY$;

call multi_insertion();
select * from emp e order by noemp;

create or replace procedure multi_insertion_gestion_exp_commit() 
language plpgsql as
$BODY$
DECLARE 
begin
	begin
INSERT INTO emp VALUES (1, 'nom1', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
INSERT INTO emp VALUES (2, 'nom2', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
exception
WHEN unique_violation THEN INSERT INTO emp VALUES (5, 'nom5', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
end; 
commit;
begin
INSERT INTO emp VALUES (3, 'nom3', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
INSERT INTO emp VALUES (1102, 'nom1102', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
INSERT INTO emp VALUES (4, 'nom4', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
exception
WHEN unique_violation THEN INSERT INTO emp VALUES (5, 'nom5', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
end;
END 
$BODY$;

call multi_insertion_gestion_exp_commit();



call multi_insertion(1);
select * from emp e order by noemp;

create or replace procedure multi_insertion_commit() 
language plpgsql as
$BODY$
DECLARE 
BEGIN
INSERT INTO emp VALUES (1, 'nom1', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
INSERT INTO emp VALUES (2, 'nom2', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
commit;
INSERT INTO emp VALUES (3, 'nom3', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
INSERT INTO emp VALUES (1102, 'nom4', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
INSERT INTO emp VALUES (4, 'nom5', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
END
$BODY$;

call multi_insertion_commit();
select * from emp e order by noemp;

create or replace procedure multi_insertion_rollback() 
language plpgsql as
$BODY$ 
BEGIN
INSERT INTO emp VALUES (1, 'nom1', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
INSERT INTO emp VALUES (2, 'nom2', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
ROLLBACK;
INSERT INTO emp VALUES (3, 'nom3', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
INSERT INTO emp VALUES (4, 'nom5', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
END
$BODY$;

call multi_insertion_rollback();

create or replace procedure multi_insertion_rollback2() 
language plpgsql as
$BODY$ 
BEGIN
INSERT INTO emp VALUES (1, 'nom1', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
INSERT INTO emp VALUES (2, 'nom2', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
commit;
INSERT INTO emp VALUES (3, 'nom3', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
INSERT INTO emp VALUES (1102, 'nom4', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
INSERT INTO emp VALUES (4, 'nom5', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
EXCEPTION
 WHEN unique_violation THEN INSERT INTO emp VALUES (5, 'nom5', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
END 
$BODY$;
call multi_insertion_rollback2();
