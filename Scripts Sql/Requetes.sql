
create or replace procedure variable() as
$BODY$

declare
	id CONSTANT integer not null :=12;
	nom varchar :='TOTO' ;
	date_embauche DATE default now();
	salaire numeric = 10000;

begin
	raise notice 'id : %, nom : %, date_embauche : %, salaire : %',id,nom,date_embauche,salaire;
end 

$BODY$
language plpgsql;
call variable();


-- 125 : Supprimer tous les employ�s ayant un A dans leur nom. Faire un SELECT sur votre
-- table pour v�rifier cela. Annuler les modifications et v�rifier que cette action s�est bien
-- d�roul�e.
delete from emp where nom like '%A%';
select*from emp where nom like '%A%';

-- 124 : Ins�rer le salari� dont le nom est MOYEN, pr�nom Toto, no 1010, embauch� le 12/12/99,
-- sup�rieur 1000, pas de comm, service no 1, salaire vaut le salaire moyen des employ�s.
insert into emp values (1010,'MOYEN','TOTO','CHAT', 1000,'1999-12-12',  (select round(cast(avg(d.sal)as numeric),2) from emp d), null,1);

delete from emp where nom ='MOYEN';
--123 : Effacer les employ�s ayant le m�tier de SECRETAIRE.
delete from emp where emploi='SECRETAIRE';

delete from emp where emploi='CHAT';
select*from emp where emploi='CHAT';

--122 : Ins�rez vous comme nouvel employ� embauch� aujourd�hui au salaire que vous d�sirez.
insert into emp values (1001,'ZENINA','ALEXANDRA','CHAT', null,'2019-10-25', 50000, null,1);
select*from emp;

--121 : Augmenter de 10% ceux qui ont un salaire inf�rieur au salaire moyen.
select e.* from emp e where e.sal<(select avg(e2.sal) from emp e2);
--UPDATE
update emp set sal = sal +(0.1*sal) where  sal<(select avg(sal) from emp);

------------------------------------------------------------------------------------------SELECT----------


--120 : Afficher l'effectif, la moyenne et le total pour les salaires et les commissions par emploi.
select emploi, count(*), avg (sal), sum(sal), avg (coalesce(comm,0)), sum(coalesce(comm,0)) from emp e group by 1;

--119 : Afficher, sur la m�me ligne, pour chaque service, le nombre d'employ�s ne touchant pas
-- de commission et le nombre d'employ�s touchant une commission.

select s.service, count(*), count(*)-count((e.comm)>=0) as "commission null", count(comm>=0) from emp e inner join serv s on e.noserv=s.noserv group by 1;

--118 : S�lectionner les emplois ayant un salaire moyen sup�rieur au salaire moyen des
--directeurs.
select e.emploi from emp e group by e.emploi having avg(e.sal)>(select avg(e2.sal)from emp e2 where e2.emploi='DIRECTEUR');

-- 117 : S�lectionner les services ayant une commission moyenne sup�rieure au quart du salaire
--moyen.
select s.service, avg(coalesce(e.comm,0)) as commission_moyenne, (avg(e.sal)/4) as quart_Salaire  from emp e 
inner join serv s on e.noserv=s.noserv group by s.service having avg(coalesce(e.comm,0))> avg(e.sal)/4 ;

------------------------------------------?-?----------------------------------------------------------
-- le null ici existe pas donc seules les valeurs existantes sont prises en compte
select s.service, avg(e.comm) as commission_moyenne, (avg(e.sal)/4) as quart_Salaire  from emp e 
inner join serv s on e.noserv=s.noserv group by s.service having avg(e.comm)> avg(e.sal)/4 ;
------------------------------------------------------------------------------------------------------
--correction 117 :
select s.noserv as salaire_moyen  from emp e 
inner join serv s on e.noserv=s.noserv group by s.noserv having avg(coalesce(e.comm,0))> avg(e.sal)/4 ;


--116 : S�lectionner les services ayant au moins deux vendeurs.
select s.service, count(*)as effectif from emp e inner join serv s on e.noserv=s.noserv where e.emploi='VENDEUR' group by s.service having count(e.emploi)>=2;


--115 : Afficher l'emploi, l'effectif, le salaire moyen pour tout type d'emploi ayant plus de deux
--repr�sentants.
select e.emploi,  count(*) as effectif, avg(e.sal) as salaire from emp e group by e.emploi having count(*)>=2 ;

--114 : Idem en rempla�ant le num�ro de service par le nom du service.
select  s.service, count(*) as effectif,  e.emploi, avg(e.sal) as salaire_moyen  from emp e 
inner join serv s on e.noserv=s.noserv group by s.service, e.emploi;

--113 : Grouper les employ�s par service et par emploi � l'int�rieur de chaque service, pour
--      chaque groupe afficher l'effectif et le salaire moyen.

select s.noserv,  s.service, count(*)as effectif,  e.emploi, avg(e.sal) as salaire_moyen  from emp e 
inner join serv s on e.noserv=s.noserv group by s.noserv, e.emploi order by s.noserv;

--correction tableau 113

select e.noserv, count(*)as effectif,  e.emploi, avg(e.sal) as salaire_moyen  from emp e group by e.noserv, e.emploi;

--112 : Pour chaque service donner le salaire annuel moyen de tous les employ�s qui ne sont ni
--pr�sident, ni directeur.
select s.service,  round(avg (e.sal*12))revenu_Annuel from emp e 
inner join serv s on e.noserv=s.noserv where e.emploi not in ('DIRECTEUR','PRESIDENT') group by s.service  ;


--111 : Pour chaque service, afficher son N� et le salaire moyen des employ�s du service arrondi
--l�euro pr�s.

select s.service, s.noserv, round(avg (e.sal)) as salaire_moyen from emp e 
inner join serv s on e.noserv=s.noserv group by s.noserv; 

-- version Tableau
select e.noserv, round (avg(e.sal)) from emp e group by e.noserv; 

-- version grouper par noserv, ville, service
select s.service, s.noserv, round(avg (e.sal)), s.service, s.ville as salaire_moyen from emp e 
inner join serv s on e.noserv=s.noserv group by s.noserv, s.service, s.ville ; 

--110 : D�terminer le nombre d'employ�s du service N�3
select count(*) from emp where noserv =3;

--109 : D�terminer le nombre d'emploi diff�rents du service N�1.
select count(distinct e.emploi) from emp e where e.noserv =1;

--108 : D�terminer le nombre d'employ�s du service 3 qui re�oivent �ventuellement une commission.
select count(*) from emp e where e.comm is not null and e.noserv=3;

--107 : S�lectionner nom, emploi, revenu mensuel de l'employ� qui gagne le plus.
select e.nom, e.emploi, (e.sal+coalesce(e.comm,0)) from emp e where (e.sal+coalesce(e.comm,0)) = (select max(e2.sal+coalesce(e2.comm,0)) from emp e2) ;

--106 : Afficher le nombre de lettres du service dont le nom est le plus court.
select min(length(service))from serv;

--105 bis : Idem mais en affichant que ceus sup�rieurs � 2
select to_char(e.embauche, 'yyyy') as "Annee", count(*) as "Nombre employes" from emp e group by to_char(e.embauche, 'yyyy') having count(*)>2 order by to_char(e.embauche, 'yyyy');

--105 : Compter les employ�s embauch�s chaque ann�e.
select to_char(e.embauche,'YYYY') as "ANNEE", count(*) as "nombre embauchee" from emp e group by "ANNEE";

--104 : Afficher le plus haut salaire, le plus bas salaire, la diff�rence entre les deux.
select max(coalesce(e.sal,0)) as "Le Plus Haut Salaire", min(coalesce(e.sal,0)) as "Le Plus Bas Salaire", (max(coalesce(e.sal,0))-min(coalesce(e.sal,0)))as "Difference"  from emp e; 

--103 : Afficher la somme des salaires et la somme des commissions des vendeurs.
select  sum(coalesce(e.sal,0))as "Somme Salaires Vendeurs", sum(coalesce(e.comm,0))as "Somme Commissions Vendeurs" from emp e where e.emploi='VENDEUR'; 

--102 : Param�trer la requ�te qui pr�c�de sur l�emploi.
select*from emp where emploi= 'VENDEUR';

--101 : Afficher la moyenne des revenus (avec commission) des vendeurs.
select avg(e.sal +coalesce(e.comm,0)) from emp e where e.emploi='VENDEUR'; 

--98 : S�lectionner les employ�s ayant plus de 12 d'anciennet�
select e.nom, e.prenom, e.embauche from emp e where (date_part('year', now()) - date_part('year', e.embauche)) > 12;

--97 : S�lectionner toutes les dates d'embauche major�es de 12 ans
select e.nom, e.prenom, e.embauche, date(e.embauche + interval '12 year') from emp e;

--96 : S�lectionner les employ�s avec leur anciennet� en mois dans l'entreprise
select e.nom, e.prenom, ((date_part('year', now()) - date_part('year', e.embauche))*12 + (date_part('month', now()) - date_part('month', e.embauche)))
as "Anciennete en mois" from emp e;

--95 : S�lectionner les employ�s avec leur anciennet� en jours dans l'entreprise.
select e.nom, e.prenom, date_part('day', now() - e.embauche) as "Anciennete en jours" from emp e;
-- Autre solution
select e.nom, e.prenom, 'now' - e.embauche as "Anciennete en jours" from emp e;

--91 : M�me chose en �crivant la colonne embauche sous la forme �day dd month yyyy'
select e.nom, e.emploi, (to_char(e.embauche, 'Day DD-Month-YYYY'))as embauche from emp e where e.noserv=6

--90 : M�me chose en �crivant la colonne embauche sous la forme �dd-mm-yy�, renomm�e
--embauche
select e.nom, e.emploi, to_char(e.embauche, 'DD-MM-YY') from emp e where e.noserv=6

--89 : S�lectionner nom, emploi, date d'embauche des employ�s du service 6.
select e.nom, e.emploi, e.embauche from emp e where e.noserv=6

--87 : Afficher le nombre de lettres qui sert � �crire le nom de chaque service.
select s.service, (length(s.service))as "Total lettres"from serv s;

--86 : S�lectionner les positions des premiers M et E dans les noms des employ�s
select e.nom, position('M' in e.nom)as "position m", position('E' in e.nom) as "position e" from emp e;

--autre exemple de l'utilisation de position :
select e.nom from emp e where position('E' in e.nom)=4;
select e.nom from emp e where e.nom like '___E%';

--85 : S�lectionner les noms des employ�s sur 3 colonnes la premi�re en majuscules, la seconde avec l'initiale en 
--majuscule et le reste en minuscules, la troisi�me en minuscules.
select upper(e.nom)as "Maj", concat(upper(SUBSTR(e.nom,1,1)), lower(SUBSTR(e.nom,2,length(e.nom)))) as "MAJ prem", lower(e.nom) from emp e;

--84 : S�lectionner les employ�s embauch�s en 1988.
select*from emp e inner join serv s on e.noserv=s.noserv where to_char(e.embauche, 'YYYY')='1988';

--83 : S�lectionner les noms des services en affichant que les 5 premiers caract�res.
select s.service, SUBSTR(s.service,1,5)as premieresCaract from serv s;

-- 82 : S�lectionner les employ�s en rempla�ant les noms par '*****' dans le service n�1, trier le r�sultat suivant
--le N� de service.
select e.nom,(case when e.noserv=1 then '*****'
		else e.nom
		end)as noms_Cache from emp e order by e.noserv;

--81 : S�lectionner nom, emploi pour les employ�s en ajoutant une colonne "CODE EMPLOI", trier le r�sultat sur ce code.
select e.nom, e.emploi, (case when e.emploi='PRESIDENT' then 1
when e.emploi='DIRECTEUR' then 2
when e.emploi='COMPTABLE' then 3
when e.emploi='VENDEUR' then 4
when e.emploi='TECHNICIEN' then 5
else 0
end) as "CODE EMPLOI" 
from emp e order by "CODE EMPLOI";

--80 : Concat�ner les colonnes Service et Ville en les reliant par " ----> ", les premi�res lettres des noms de villes 
--     doivent se trouver sur une m�me verticale
select service, ville, 
concat(rpad (service,(select max(length(service))+2 from serv),'--'),'-->',ville) from serv;

select nom||prenom from emp;
--79 : Idem sans arrondir mais en tronquant.
select e.nom, e.prenom, e.emploi, trunc (cast(e.sal/151.67*7 as numeric),2)as "Salaire Journalier", trunc(cast(e.sal/151.67 as numeric),2)as "Salaire Horaire"from emp e where e.noserv in (3,5,6);

--78 : Afficher le nom, l'emploi, les salaires journalier et horaire pour les employ�s des services
--     3,5,6 (22 jours/mois et 8 heures/jour), sans arrondi, arrondi au centime pr�s.

select e.nom, e.prenom, e.emploi, e.sal/22 as "Salaire Journalier sans arrondi", round(cast(e.sal/22 as numeric),2)as "Salaire Journalier arrondi", e.sal/22/8 as "Salaire Horaire non arrondi", round(cast(e.sal/22/8 as numeric),2)as "Salaire Horaire"from emp e where e.noserv in (3,5,6);
--77 : Idem pour les employ�s des services 3,5,6 en rempla�ant GAIN_ MENSUEL par GAIN
--MENSUEL
select e.nom, e.prenom, e.emploi, e.sal as "SALAIRE", e.comm as "COMMISSION", SAL+COALESCE(COMM,0) as "GAIN MENSUEL"  from emp e where e.noserv in (3,5,6);

--76 : Idem pour les employ�s des services 3,5,6 en rempla�ant les noms des colonnes : SAL par
-- SALAIRE, COMM par COMMISSIONS, SAL+COALESCE(COMM,0) par GAIN_MENSUEL.
select e.nom, e.prenom, e.emploi, e.sal as "SALAIRE", e.comm as "COMMISSION", SAL+COALESCE(COMM,0) as "GAIN_MENSUEL"  from emp e where e.noserv in (3,5,6);

--75 : S�lectionner nom, pr�nom, emploi, salaire, commissions, revenu mensuel pour les employ�s
--des services 3,5,6
select e.nom, e.prenom, e.emploi, e.sal, e.comm,(e.sal+COALESCE(e.comm,0))revenu_mensuel from emp e where e.noserv in (3,5,6);

--74 : S�lectionner le nom, l�emploi, le service et le revenu annuel ( � l�euro pr�s) de tous les
--vendeurs.
select e.nom, e.emploi, s.service, round(cast(e.sal*12 as numeric),0)revenu_Annuel from emp e inner join serv s on e.noserv=s.noserv where e.emploi='VENDEUR';

--73 : S�lectionner nom, pr�nom, emploi, le pourcentage de commission (deux d�cimales) par
--rapport au revenu mensuel ( renomm� "% Commissions") , pour tous les vendeurs dans l'ordre
-- d�croissant de ce pourcentage.
select e.nom, e.prenom, e.emploi, round(cast(e.comm/e.sal*100 as numeric),2) as "% commission"  from emp e where e.emploi='VENDEUR' order by "% commission" desc;

--72 : S�lectionner le nom, le salaire, commission des employ�s dont la commission repr�sente plus que le double du salaire.
select e.nom, e.sal, e.comm from emp e where e.comm > e.sal*2;

--71 :S�lectionner le nom, l�emploi, le revenu mensuel (nomm� Revenu) avec deux d�cimales pour tous les employ�s, dans l�ordre des revenus d�croissants.
select e.nom, e.emploi, round(cast(e.sal as numeric),2)revenu from emp e order by revenu desc;

--70 : S�lectionner le nom, le pr�nom, le service, le revenu des employ�s qui ont des subalternes, trier le r�sultat suivant le revenu d�croissant.
select e.nom, e.prenom, e.noserv, e.sal from emp e  where e.noemp in(select a.sup from emp a )order by sal desc;

--69 : S�lectionner le nom, l�emploi, la ville pour les employ�s qui ne travaillent pas dans le
--m�me service que leur sup�rieur hi�rarchique direct
select e.nom, e.emploi, s.ville, s.service from emp e inner join serv s on e.noserv=s.noserv where e.noserv not in(select f.noserv from emp f where e.sup=f.noemp);

-- 68 : S�lectionner les employ�s du service INFORMATIQUE embauch�s la m�me ann�e
-- qu'un employ� du service VENTES. ( ann�e -> oracle : to_char(embauche,�YYYY�)> MYSQL: DATE_FORMAT(embauche,�%Y�)
select*from emp e inner join serv s on e.noserv=s.noserv where s.service ='INFORMATIQUE' and to_char(e.embauche,'YYYY')in (select to_char(c.embauche,'YYYY') from emp c inner join serv t on c.noserv=t.noserv where t.service ='VENTES' );

-- 67 : S�lectionner les employ�s dont le salaire est plus �lev� que le salaire moyen de leur
--service (moyenne des salaires = avg(sal)), r�sultats tri�s par num�ros de service.
select*from emp e where sal>(select avg(sal)from emp d where e.noserv=d.noserv) order by noserv;

--version marine
select e.* from emp e,(select t.noserv, avg(t.sal) as moy from emp t group by t.noserv)moyservice where e.noserv=moyservice.noserv and e.sal>moyservice.moy order by noserv;

--66 : S�lectionner les employ�s de LILLE ayant le m�me emploi que RICHARD, trier le
--r�sultat dans l'ordre alphab�tique des noms.
select*from emp e where emploi=(select d.emploi from emp d where nom='RICHARD') order by nom asc;

--65 : S�lectionner les employ�s du service N�1 ayant le m�me emploi qu'un employ� du service des VENTES.
select*from emp e where e.noserv=1 and e.emploi in (select d.emploi from emp d, serv s where  d.noserv=s.noserv and s.service='VENTES');

--64 : S�lectionner nom, pr�nom, emploi, salaire pour les employ�s ayant m�me emploi et un salaire sup�rieur � celui de CARON.
select e.nom, e.prenom, e.emploi,e.sal from emp e where e.emploi=(select d.emploi from emp d where nom='CARON' and e.sal>d.sal); 

--63 : S�lectionner les employ�s du service 1 dont l'emploi n'existe pas dans le service 3.
select*from emp e where  e.noserv =1 and e.emploi not in(select d.emploi from emp d where d.noserv=3); 

--62 : S�lectionner les employ�s du service 1, ayant le m�me emploi qu'un employ� du service N�3
select*from emp e where  e.noserv =1 and e.emploi in(select d.emploi from emp d where d.noserv=3); 

--61 : S�lectionner les noms, le num�ro de service, l�emploi et le salaire des
--personnes travaillant dans la m�me ville que HAVET.
select e.nom, e.noserv, e.emploi, e.sal from emp e inner join serv s on e.noserv=s.noserv  where s.ville =(select t.ville from  serv t, emp d where d.noserv=t.noserv and d.nom='HAVET');

--60 : S�lectionner le nom, le pr�nom, le revenu mensuel des employ�s qui gagnent plus
--qu'au moins un employ� du service N�3, trier le r�sultat dans l'ordre croissant des
--revenus mensuels.
select e.nom,e.prenom, e.sal from emp e where sal>(select min(d.sal) from emp d where d.noserv=3) order by sal asc;

--59 : S�lectionner le nom, le pr�nom, la date d�embauche des employ�s plus anciens que tous les employ�s du service N�6. (Attention MIN)
select e.nom, e.prenom, e.embauche from emp e where embauche < (select min(d.embauche) from emp d where d.noserv=6);

--58 : S�lectionner les nom et date d'embauche des employ�s plus anciens que MINET, dans l�ordre des embauches.
select e.nom, e.prenom, e.embauche from emp e where embauche < (select embauche from emp d where nom='MINET') order by e.embauche asc;

--57 : S�lectionner les employ�s embauch�s le m�me jour que DUMONT.
select*from emp e where e.embauche in (select embauche from emp d where nom ='DUMONT');

--56 : S�lectionner les employ�s qui ont le m�me chef que DUBOIS, DUBOIS exclu.
select*from emp e where e.sup =(select sup from emp d where nom ='DUBOIS')and e.nom!='DUBOIS';

--55 : S�lectionner les employ�s qui travaillent � LILLE.
select nom, prenom from emp e where exists (select*from serv s  where e.noserv=s.noserv and ville ='LILLE');

--54 : S�lectionner les services ayant au moins un employ�.
select s.service from serv s where exists (select*from emp e where e.noserv=s.noserv);

--53 : S�lectionner les num�ros de services n�ayant pas d�employ�s sans une jointure externe
select distinct s.noserv from serv s where  not exists (select e.noserv from emp e where e.noserv=s.noserv);


--52 : S�lectionner sans doublon les pr�noms des directeurs et � les pr�noms des techniciens du service 1 � avec un UNION.
 select distinct c.prenom from (select prenom,emploi from emp where emploi = 'DIRECTEUR' union select prenom,emploi from emp where emploi = 'TECHNICIEN' and noserv = 1) as c; 
 
--51 : S�lectionner les nom et date d'embauche des employ�s suivi des nom et date
--d'embauche de leur sup�rieur pour les employ�s plus ancien que leur sup�rieur, dans l'ordre
--nom employ�s, noms sup�rieurs

-- ! ! ! ici relation reflexive on cr�e deux tables distinctes pour pouvoir comparer
 select e.nom, e.embauche, s.nom, s.embauche from emp as e, emp as s where e.sup = s.noemp and e.embauche < s.embauche order by	s.nom desc;

--50 : S�lectionner le nom, l'emploi, suivis de toutes les colonnes de la table SERV pour tous les employ�s.
 select e.nom,e.emploi, s.*from	emp e,serv s where e.noserv = s.noserv;

--49 : Idem en utilisant des alias pour les noms de tables.
 select	nom, emploi, e.noserv, s.service from emp as e, serv as s where e.noserv = s.noserv;

--48 : S�lectionner le nom, l'emploi, le num�ro de service, le nom du service pour tous les employ�s.
 select	nom, emploi, emp.noserv, serv.service from emp, serv where emp.noserv = serv.noserv;

--47 : S�lectionner le nom, le pr�nom, l'emploi, le nom du service de l'employ� pour tous les employ�s.
 select nom, prenom, emploi, s.service from emp as e, serv as s where s.noserv = e.noserv;

--43 : Trier les employ�s (nom, pr�nom, n� de service, salaire, emploi) par emploi, et pour chaque emploi par ordre d�croissant de salaire.
 select nom, prenom, noserv, sal, emploi from emp order by 	emploi desc;

--42 : Idem en indiquant le num�ro de colonne � la place du nom colonne.
 select nom from emp as "4";

--41 : Trier les employ�s (nom, pr�nom, n� de service , salaire) du service 3 par ordre de salaire d�croissant.
 select nom, prenom, noserv, sal from emp where noserv = 3 order by sal desc;

--40 : Trier les employ�s (nom, pr�nom, n� de service, salaire) du service 3 par ordre de salaire croissant.
 select	nom, prenom, noserv, sal from emp where noserv = 3 order by sal asc;

--39 : S�lectionner les employ�s dont le nom ne s'�crit pas avec 5 caract�res.
 select*from emp where nom not like '_____';

--38 : S�lectionner les employ�s dont la troisi�me lettre du nom est un R
select*from emp where nom like '__R%';

--37 : S�lectionner les employ�s dont le nom s'�crit avec 6 caract�res et qui se termine par N.
 select*from emp where nom like '_____N';

--36 : S�lectionner les employ�s ayant au moins un N et un O dans leur nom.
 select*from emp where nom like '%N%' and nom like '%O%';

--35 : S�lectionner les employ�s ayant exactement un E dans leur nom.
 select*from emp where nom like '%E%' and nom not like '%E%E%';

--34 : S�lectionner les employ�s ayant au moins deux E dans leur nom.
select*from emp where nom like '%E%E%';

--33 : S�lectionner les employ�s dont le nom se termine par T.
 select*from emp where nom like '%T';

--32 :S�lectionner les employ�s dont le nom commence par la lettre M
 select*from emp where nom like 'M%';

--31 : S�lectionner les directeurs des services 2 ,3 , 4, 5 sans le IN.
 select*from emp where emploi = 'DIRECTEUR' and noserv between 2 and 5;

--30 : S�lectionner les employ�s qui ne sont pas directeur et qui ont �t� embauch�s en 88.
select*from emp where  emploi != 'DIRECTEUR' and (embauche between '1988-01-01' and '1988-12-31');

--29 : S�lectionner les employ�s qui gagnent moins de 20000 et plus de 40000 euros.
 select*from emp where sal not between 20000 and 40000;

--28 : S�lectionner les employ�s qui gagnent entre 20000 et 40000 euros, bornes comprises
select*from emp where sal between 20000 and 40000;

--27 : S�lectionner les subalternes qui ne sont pas dans les services 1, 3, 5.
 select*from emp where emploi not in ('DIRECTEUR','PRESIDENT')and noserv not in (1,3,5);

select*from emp where emploi != 'DIRECTEUR' and emploi != 'PRESIDENT' and noserv not in (1,3,5);

--26 : S�lectionner les directeurs des services 2, 4 et 5.
 select*from emp where emploi = 'DIRECTEUR' and noserv in(2, 4,5);

--25 : S�lectionner les employ�s qui ne sont ni technicien, ni comptable, ni vendeur.
 select*from emp where emploi not in('TECHNICIEN', 'COMPTABLE', 'VENDEUR');

--24 : S�lectionner les employ�s qui sont techniciens, comptables ou vendeurs
 select*from emp where emploi in ('TECHNICIEN', 'COMPTABLE', 'VENDEUR');

--23 : S�lectionner les employ�s qui ne sont ni directeur, ni technicien et travaillant dans le service 1.
 select*from emp where noserv = 1 and (emploi != 'DIRECTEUR' and emploi != 'TECHNICIEN');

--22 : S�lectionner les employ�s du service 1 qui sont directeurs ou techniciens.
 select*from emp where noserv = 1 and (emploi = 'DIRECTEUR' or emploi = 'TECHNICIEN');

--21 : S�lectionner les � directeurs et les techniciens � du service 1.
select*from emp where (emploi = 'DIRECTEUR' or emploi = 'TECHNICIEN') and noserv = 1;

--20 : S�lectionner les directeurs et � les techniciens du service 1 �.
select*from emp where emploi = 'DIRECTEUR' or emploi = 'TECHNICIEN' and noserv = 1;

--19.syntaxe !=: S�lectionner les directeurs qui ne sont pas dans le service 3.
 select*from emp where noserv != 3 and emploi = 'DIRECTEUR';

--19.syntaxe <> : S�lectionner les directeurs qui ne sont pas dans le service 3.
 select*from emp where noserv <> 3 and emploi = 'DIRECTEUR';

--18 : S�lectionner tous les pr�sidents et directeurs.
 select*from emp where emploi = 'DIRECTEUR' or emploi = 'PRESIDENT';

--17 : S�lectionner les vendeurs du service 6 qui ont un revenu mensuel sup�rieur ou �gal � 9500 �.
 select*from emp where emploi = 'VENDEUR' and noserv = 6 and sal>9500;

--16 : S�lectionner les noms, emploi, salaire, num�ro de service de tous les employ�s du service 5 qui gagnent plus de 20000 �.
 select nom, emploi, sal,noserv from emp where noserv = 5 and sal>20000;

--15 : S�lectionner les employ�s qui n�ont pas de chef.
 select*from emp where sup isnull;

--14 : S�lectionner les employ�s qui ont un chef.
 select*from emp where sup notnull;

--(ici relation reflexive)
--13 : S�lectionner les employ�s qui touchent �ventuellement une  commission et dans l�ordre croissant des commissions.
 select*from emp where comm notnull order by comm asc;

--12 : S�lectionner les employ�s qui ne touchent jamais de commission.
 select*from emp where comm isnull;

--11 : S�lectionner les employ�s dont la commission est inf�rieure au salaire. V�rifiez le r�sultat jusqu�� obtenir la bonne r�ponse.
 select*from emp where comm<sal;

--10 : S�lectionner les noms, num�ros de service de tous les services dont le num�ro est inf�rieur ou �gal � 2
 select service from serv where noserv <= 2;

--9 : S�lectionner les noms(gaffe ici nom =service), num�ros de service de tous les services dont le num�ro  sup�rieur � 2.
 select	service, noserv from serv where noserv>2;

--8 : S�lectionner les noms, pr�noms, num�ro d�employ�, num�ro de service de tous les techniciens.
 select nom,prenom,noemp,noserv from emp where emploi = 'TECHNICIEN';

--7 : S�lectionner les employ�s du service N�3.
 select*from emp where noserv = 3;

--6 : S�lectionner les diff�rents emplois de la table EMP.
 select distinct emploi from emp;

--5 : S�lectionner les emplois de la table EMP
 select emploi from emp;

--3 : selectionner les tables noserv et service de la table serv
 select	noserv,	service from serv;
 
--2 : selectionner tous les champs de la table serv
 select	noserv, service,ville from	serv;

--1 : selection de tous les colonnes de la tables serv:
 select*from serv;
