---------------------------------------
--------------------------------------- Procedure qui affiche Hello
---------------------------------------

create or replace procedure afficherHello()
language plpgsql
as $body$
declare
mot varchar:='Hello !';
begin
	raise notice '%',mot;
end
$body$

call afficherHello();

----------------------------------------
---------------------------------------- Afficher bonjour 10 fois
----------------------------------------

create or replace procedure afficherBonjourDix()
language plpgsql
as $body$
declare
mot varchar:='Bonjour';
begin
	for i in 1..10 loop
	raise notice '%',mot;
	end loop;
end
$body$
call afficherBonjourDix();

----------------------------------------
---------------------------------------- Addition chiffres en parametres
----------------------------------------

create or replace procedure addition(nb1 integer,nb2 integer)
language plpgsql
as $body$
declare
resultat integer;
begin
resultat=nb1+nb2;
raise notice '%',resultat;

end
$body$

call addition(1,2);

----------------------------------------
---------------------------------------- Parcourir une requete et qui affiche tout des emp ou le nom depasse un certain nombre
----------------------------------------

create or replace procedure afficherInfosv2()
language plpgsql
as $body$
declare
pointeur cursor for select*from emp;
ligne record;

begin
	
open pointeur ;
	loop
		fetch pointeur into ligne;
			EXIT when not found;
		if length(ligne.nom)>3 then
				raise notice '%',  ligne;
		end if;
	end loop;
close pointeur;
end;
$body$;
call afficherInfosv2();


raise notice '%',resultat;

end
$body$

call addition(1,2);

------------------------------exercice1 Diaporama :Ins�rer un employ� dans la Bdd, mettre en param�tres de la proc�dure son id, son nom et son prenom.


create or replace procedure exe1diapo(id integer,nom varchar,prenom varchar)
language plpgsql as
$BODY$

declare
	begin
	INSERT INTO emp VALUES (id, nom, prenom, 'PROGRAMMEUR','1500', now(), 40000, 5000,5);

end; 
$BODY$

call exe1diapo(1200,'Maious','Truck');


------------------------------- exercice 2 Diaporama : Augmenter le salaire de 10% de l'employ� dont le noemp est pass� en param�tre jusqu'�
------------------------------- ce qu'il devient l'employ� le mieux pay�. A
------------------------------- chaque tour de boucle afficher le salaire augment�

create or replace procedure exercice2Diapo(id integer) as
$BODY$
declare
	sal_max integer;
	salc integer;
begin
	--on met le salaire max dans sal_max de la table emp
	select max(sal) into sal_max from emp;
	
	-- on met le salsaire ou noemp=id dans salc
	select sal into salc from emp where noemp=id;
	
	-- On compare le salaire salc avec sal_max et on boucle tanque le salc et < au sal_max
	while  salc < sal_max loop
	
		-- on met � jour le salaire(sal=sal*1.1) ou le noemp=id dans la table emp
		update emp set sal = sal*1.1 where noemp=id;

		-- on insere le salaire augmente dans salc pour continuer la comparaison
		select sal into salc from emp where noemp=id;
	
		-- affichage console de l'augmentation du salaire � chaque boucle
		raise notice 'sal %',salc;
	
	end loop;
end 
$BODY$
language plpgsql;

call exercice2Diapo(1200);

update emp set sal = 2000 where noemp='1200';

-------------------  A l'aide d'un curseur parcourir l'ensemble des employ�s:
-------------------* si leur id est pair et leur salaire est Sup�rieur � la moyenne
-------------------  remplacer leur pr�nom par Pierre, 
-------------------* si leur id est pair et que leur salaire et inf�rieur ou �gal � la moyenne des salaires remplacer leur pr�nom par Paul
-------------------  Sinon remplacer leur pr�nom par Jacques

create or replace procedure exercice3Diapo() as
$BODY$
declare
	
	moy integer;
	curs cursor for select * from emp e;
	cible record;

begin
	--selection du salaire moyen dans la table emp
	select avg(sal) into moy from emp;

	--ouverture cursor
	open curs;
	--on positionne le cursor dans la cible record
	fetch curs into cible;
	--tant qu'on recupere les resultats
	while found loop 
		--si la le noeemp recuper� dans la ligne cible est pair et le salare dans la cible est sup � la moyenne alors:
	if cible.noemp%2=0 and cible.sal>moy then
		
		--on met � jour la table emp ou se trouve actuellement le cursor
		UPDATE emp SET prenom='Pierre' WHERE CURRENT OF curs;
	
	--sinon si le noemp est seulement pair alors: 
	elsif cible.noemp%2=0 then
			
			--mets � jour et on met le prenom Paul dans la position actuellee du cursor
			UPDATE emp SET prenom='Paul' WHERE CURRENT OF curs;
	
		--Sinon	
	else 
			UPDATE emp SET prenom='Jacques' WHERE CURRENT OF curs;
	
	end if;
fetch curs into cible;
	end loop;
close curs;
end 
$BODY$
language plpgsql;

call exercice3Diapo();


-----------------------Faire un insert de n employes, l'id de d�but et de l'id de fin sont pass�es en parametres
-----------------------Apres l'insertion, si l'id est pair l'enregistrer dans la bdd
-----------------------S'il est impair ne pas l'enregistrer dans la bdd
create or replace procedure exercice4Diapo(id_debut integer, id_fin integer) as
$BODY$
declare
begin
	
	for i in id_debut..id_fin loop
	INSERT INTO emp VALUES (i, 'nom1', 'prenom', 'PROGRAMMEUR','1500', now(), 40000, 5000,5);
	if i%2=0 then
		commit;
	else
		rollback;
	end if;
	end loop;
end
$BODY$
language plpgsql;

call exercice4Diapo(20,30);

----------------------------------- Exercice 1 Fonctions

create or replace function calculDir() returns integer
language plpgsql
as
$Fonction$

begin
	--ici on retourne le nombre d'employes ayant l'emploi de directeur la requete retourne uin entier
	return (select count(*) from emp where emploi='DIRECTEUR');
end;
$Fonction$

-- procedure qui va appeler la fonction et gerer l'affichage 
create or replace procedure afficherTotalDir()
language plpgsql
as
$procedure$
begin
raise notice 'Total directeurs : %', (select calculDir()); 
end;
$procedure$

appel
call afficherTotalDir();

------------------------------------Exercice 2 Fonctions

create or replace function versEuro(taux numeric, salaire numeric) returns numeric
language plpgsql
as
$VersEuro$

declare

begin

return salaire*taux;

end;
$VersEuro$


create or replace procedure afficheSalaireFr(id integer)
language plpgsql
as
$procedure$
declare 
c cursor select sal from emp e;
ligne record;

begin
	fetch c into ligne
	
	
raise notice 'Salaire : %',(select versEuro(0.1557,59650)); 
end;
$procedure$

call afficheSalaireFr();


|--------------------------------------------------------------------------|
|------------------------------- TABLEAUX ---------------------------------|
|--------------------------------------------------------------------------|
				
------------------------- EXO 1 :
------------------------- Fonction qui retourne un tableau et une proc�dure qui le lit:

create or replace function ftableau() returns int[]  as
$body$
declare
test int[];
begin
	test[1]=10;
	test[2]=20;
	--test[0]=0;
	return test;
end
$body$
language plpgsql;
--appel fonction
select ftableau();


create or replace procedure ptableau()   as
$body$
declare
test int[];
begin
 test=ftableau(); 	raise notice '0: %, 1 : %, 2: %',test [0], test[1],test[2];
 end
$body$
language plpgsql;

--appel procedure
call ptableau();


-------------------------------------------------	Exo2Ecrire une fonction qui prend en param�tre 
-------------------------------------------------	un tableau de num�ros d'employ�s et qui retourne un tableau
-------------------------------------------------	dont chaque �l�ment est une chaine de caract�res sous une forme 
-------------------------------------------------	concat�n�e des informations de l'employ�.
------------------------------------------------- 	ex : "noemp,INITIALE,sal,noserv,ville"

--fonction
create or replace function tablEnParam(id integer[])
returns text[]
language plpgsql
as
$$
declare

tablInfo text[];
ligneTable record;
compteur integer;
infoConcat text;

begin
	foreach compteur in array id loop
	select noemp,nom,sal,noserv,ville from emp natural inner join serv where noemp = compteur into ligneTable;

		 infoConcat = ligneTable.noemp||','||ligneTable.nom||','||ligneTable.sal||','||ligneTable.noserv||','||ligneTable.ville ;
	
		 tablinfo= array_append(tablInfo,infoConcat);
	end loop;
raise notice '%', tablinfo;
return tablInfo;
end
$$

--procedure
create or replace procedure recupTab()
language plpgsql
as
$$
declare

tableauId integer [];

c cursor for select noemp from emp;
rec_id record;
begin
	open c;
	loop
	fetch  c into rec_id;
	exit when not found;
	tableauId=array_append(tableauId,rec_id.noemp);

	end loop;
	close c;
	perform tablEnParam(tableauId);
	
end;
$$

--appel
call recupTab();



------------------------------------------------ exemple pourcreer un  tableau � double dimension
--la fonction 
create or replace function mieux() returns int[][]
language plpgsql
as $mieuxpaye$
declare
	test int[][];
begin
	test := array[array[1,2,3], array[4,5,6]];
	return test;
end
$mieuxpaye$;

--procedure qui appele la fonction
create or replace procedure affichemieux()
language plpgsql
as $mieux$
	declare

		begin
			raise notice '%',mieux();
		end
	$mieux$;

--appelede la procedure
call affichemieux();


---------------------------------------------------------------------
--------------------------------------------------------------------- EXO 3 pareil que EXO 2 mais ave un tableau a double dimension
---------------------------------------------------------------------
--fonction
create or replace function tablDeuxDim(id integer[])
returns text[][]
language plpgsql
as
$$
declare

tablInfo text[][];
ligneTable record;
compteur integer;
infoConcat text;

begin
	foreach compteur in array id 
	
	--debut boucle
	loop
	
	select noemp,nom,sal,noserv,ville from emp natural inner join serv where noemp = compteur into ligneTable;
	
	-- on concate les donnes recup dans la ligne avec  || et on init la variable infoconcat
	infoConcat = ligneTable.nom||','||ligneTable.sal||','||ligneTable.noserv||','||ligneTable.ville ;
	
	--init du tableau a double dimension
	tablInfo = tablInfo||array[array[''||ligneTable.noemp], array[infoConcat]];

	end loop;

raise notice '%', tablinfo;
return tablInfo;
end;
$$


--procedure qui appele la fonction
create or replace procedure recupTousNumEmp()
language plpgsql
as
$$
declare

tableauId integer [];

c cursor for select noemp from emp;
rec_id record;
begin
	open c;
	loop
	fetch  c into rec_id;
	exit when not found;
	tableauId=array_append(tableauId,rec_id.noemp);

	end loop;
	close c;
	perform tablDeuxDim(tableauId);
	
end;
$$

--appel de la procedure
call recupTousNumEmp();

|--------------------------------------------------------------------------|
|------------------------------- TRIGGERS ---------------------------------|
|--------------------------------------------------------------------------|

-------------------------------------------------Exercice Triggers

--creation d'une table ou on va enregistrer les evenements pour le trigger
create table emp_audit(
id int primary key not null,
utilisateur text,
entry_date date,
operation text
);

--creation d'une fonction qui va etre effectu� quand le trigger vase declencher
create or replace function auditInsertion() returns trigger as 
$a$
begin 
		if new.sal<10000 then
			
			insert into emp_audit(id,utilisateur, entry_date, operation)values (new.noemp, current_user, current_timestamp, TG_OP );
			raise notice 'Seti a dit de ne pas le mettre !';
			return new;
		end if;
end
$a$
language plpgsql;

--creation du trigger qui va se declencher avant l'insertion ou un update sur la table emp
createtrigger testTrigger before insert or update on emp for each row execute procedure auditInsertion();

-- requete test pour le trigger
insert into  emp VALUES (8875, 'Testeur', 'Trigger', 'Cobaye','1500', now(), 400, 50,5);


|--------------------------------------------------------------------------|
|-------------------------------  SCHEMA  ---------------------------------|
|--------------------------------------------------------------------------|
--Gestion de l'acces aux donn�es

--indique le chemin utilise pour pointer le schema utilis�
show search_path;

--modifier le chemin
set search_path to public, public;

--Creation de schema
create schema schema_test;

-- Creation de table dans le schema:
create table schema_test.personne(id int);

-- Supprimer Schema ( si ya des tables il va y avoir erreur )
drop schema schema_test;

-- Supprimer schema et de ses objets 
drop schema schema_test cascade;

--donner droit aux utilisateurs de creer ou acceder � der objets dans le schema
grant [create, usage] on schema to user;

-- Donner les permissions CRUD sur les tables d�un schema
ALTER DEFAULT PRIVILEGES IN SCHEMA my_schema
GRANT SELECT, INSERT, UPDATE, DELETE on mytable To USER;

--|--------------------------------------------------------------------------|
--|-------------------------------TABLESPACE---------------------------------|
--|--------------------------------------------------------------------------|

-- Gestion physique de l'allocation de l'espace pour 
---definir l'emplacement dans le syst�me de fichiers o� seront 
-- stock�s les fichiers repr�sentant les objets de la base de donn�es.

-- Cr�er un tablespace 'emplacement doit �tre un r�pertoire existant, 
-- Tous les objets cr��s par la suite dans le tablespace seront stock�s dans des fichiers contenus dans ce r�pertoire. 

CREATE TABLESPACE mytablespace LOCATION 'c: ENV \\'';

--Cr�er une table dans un tablespace
CREATE TABLE mytable (id int ) TABLESPACE mytablespace

--Mettre le tablespace par d�faut
SET default_tablespace = mytablespace

-- Supprimer un tablespace
DROP TABLESPACE mytablespace;

--|--------------------------------------------------------------------------|
--|-------------------------------SEQUENCES ---------------------------------|
--|--------------------------------------------------------------------------|

--creation d'une sequence nomme seqTest
create sequence seqTest as int increment 5 start 100;

-- suppression d'une sequence 
drop sequence seqTest;

--afficher la valeur suivante de la sequence (� chaque appel la valeur va augmenter ou descendre en fonction de ses parametres
select nextval('seqTest');

------------------------------------------------------------------------------ EXEMPLE d'utilisation
--on va associer une sequence � une table : 
--Creation d'une nouvelle table 
	
CREATE table schema_test.details_commande(
	-- le fait de specifier serial indique  � postgre de relier la colonne
	--� une sequence qu'il va cr�er automatiquement.
    commande_id SERIAL,
    objets_id INT NOT NULL,
    produit_id INT NOT NULL,
        prix DEC(10,2) NOT NULL,
    PRIMARY KEY(commande_id, objets_id)
);

-- Creation de la nouvelle sequence associe aux objets de la table details_commande
CREATE SEQUENCE id_commande_objets
START 10
INCREMENT 10
MINVALUE 10
OWNED by details_commande.objets_id;

--insertion des valeurs 
INSERT INTO 
   details_commande(commande_id, objets_id, produit_id,prix)
values
	--ici on fait appel a la sequence attribu� automatiquement lors de la creation de la table (elle commence par defaut � 2)
    (100, nextval('details_commande_commande_id_seq'),12,100),
    --ici on fait appel a la sequence cr�e � part et laquelle fut attribu� le nom id_commande_ojets
    (100, nextval('id_commande_objets'),12,550),
    (100, nextval('id_commande_objets'),12,250);

--requete des donn�e sur la table details_commande
SELECT
    commande_id,
    objets_id,
    prix
FROM
    details_commande; 
   

--suppression de la sequence:
DROP sequence id_commande_objets;

--suppression de la sequence si elle existe et supprime les objets associes
DROP sequence if exists id_commande_objets cascade;


--- INFOS SEQUENCES ************************************************
--affiche toutes les info sur les sequences de la base de donn�es
 SELECT * FROM information_schema.sequences;

-- informations sur la sequence dont on a mis le nom
 select*from id_commande_objets;
- ----------------************************************************



--|--------------------------------------------------------------------------|
--|-------------------------------   INDEX  ---------------------------------|
--|--------------------------------------------------------------------------|
-- Les index sont principalement utilis�s pour am�liorer les performances de la base de donn�es
--Les champs cl� pour l'index sont sp�cifi�s � l'aide de noms des colonnes ou par des expressions �crites entre parenth�ses.
--Un champ d'index peut �tre une expression calcul�e � partir des valeurs d'une ou plusieurs colonnes de la ligne de table
-- exemple:
/*
CREATE INDEX  index_name  ON table_name 
[USING method]											 <	index method such as btree, hash, gist, spgist, gin, and brin. PostgreSQL uses btree by default.
(
    column_name [ASC | DESC] [NULLS {FIRST | LAST }],
    ...
); 
*/
--voir le cheminement de la requete!
explain select*from emp where nom ='Zenina';

--creation d'un index sur la colonne nom
CREATE INDEX idx_noms ON emp(nom);
