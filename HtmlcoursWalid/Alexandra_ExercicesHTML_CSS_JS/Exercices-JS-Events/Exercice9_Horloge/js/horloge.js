afficherHeure();


function afficherHeure() {
    var date = new Date();
    var heure = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();

    heure = ajoutZero(heure);
    min = ajoutZero(min);
    sec = ajoutZero(sec);

    $("#temps").html(heure + " : " + min + " : " + sec);

    var temps = setTimeout(function () { afficherHeure() }, 500);


}

function ajoutZero(nb) {
    if (nb < 10) {
        nb = "0" + nb;
    }
    return nb;
}
