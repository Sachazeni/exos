var tbody = $('#myTable').children('tbody');
var table = tbody.length ? tbody : $('#myTable');

$("#add").click(function () {

if($("#entreUtilisateur").val()){
    var nouveauElem = $("#entreUtilisateur").val();

    var ligne = $("<tr></tr>");
    var caseTabTdCheck = $("<td></td>");


    //creee chexbox
    var checkBtn = $("<input>");
    $(checkBtn).attr("type", "checkbox");

    caseTabTdCheck.append(checkBtn);
    $(checkBtn).change(function () {
        $(this).parent().parent().find("input").toggleClass("texteBarre");
    })

    //cree input et 
    var caseTabTdInputUser = $("<td></td>");
    var inputUser = $("<input>");
    $(inputUser).attr("type", "text")
    $(inputUser).attr("class", "inputUserListe");
    $(inputUser).val(nouveauElem);

    //creation bouton
    var btnSupprimer = $("<button>Supprimer</button>")
    $(btnSupprimer).attr("type", "button");
    $(btnSupprimer).attr("class", "btn btn-outline-info");

    //permet de supprimer tous les siblings proches,
    $(btnSupprimer).click(function () {
        $(this).siblings().remove();

        //faut supprimer bouton sinon il reste
       $(this).remove();
    });

    ligne.append(caseTabTdCheck);
    ligne.append(inputUser);
    ligne.append(btnSupprimer);

    table.append(ligne);
}
else{
    alert("Rien à ajouter à la liste");
}
});

