

/**
 * Des que le champ perd son focus l'operation suivante est execute
 */
$("input[type=text]").change(function () {

    //Comparaison en faisant appel a la fonction de comparaison
    if (comparaison($("#saisieUtilisateur").val())) {
        $("#resultat").css("display", "initial");
        $("#resultat").html("Saisie correcte !");
        $("#resultat").css("color", "green");
        $("#saisieUtCopie").html(" "+$("#saisieUtilisateur").val());
    }

    //si la comparaison retourne faux
    else {
        
        $("#resultat").css("display", "initial");
        $("#resultat").html("Saisie incorrecte !");
        $("#saisieUtCopie").html(" "+$("#saisieUtilisateur").val());
        $("#resultat").css("color", "red");
    }
});

/**
 * Methode pour realiser une comparaison
 * @param {*} txt : le texe passe en parametres est comparé avec la phrase predefini
 * @returns : false si les deux phrases ne correspondent pas, true si elle correspondent
 */
function comparaison(txt) {
    var pattern = "A man with no motive is a man no one suspects. Always keep your foes confused";
    if (pattern.localeCompare(txt) === 0) {
        return true;
    }
    else {
        return false;
    }
}
