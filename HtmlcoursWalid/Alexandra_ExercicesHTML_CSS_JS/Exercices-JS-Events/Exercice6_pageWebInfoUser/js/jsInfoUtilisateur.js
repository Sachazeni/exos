
var tbody = $('#myTable').children('tbody');
var table = tbody.length ? tbody : $('#myTable');
var message = "Veuillez remplir les champs vides!";

$("#ok").click(
    function (event) {
        var genreInfo = "Homme";
        var nom;
        var prenom;
        var telephone;
        var persCharg = 0;
        var age;
        var flag = true;
        //Verification si nom pas vide
        if (!$("#nom").val()) {

            flag = false;
        } else {
            nom = $("#nom").val();
        }

        //verification si prenom pas vide
        if (!$("#prenom").val()) {
            flag = false;
        } else {
            prenom = $("#prenom").val();
        }

        //verif si le champ du telephone pas vide 
        if (!$("#tel").val()) {

            flag = false;
        } else {
            telephone = $("#tel").val();
        }
        //verif chalos age pas vide
        if (!$("#age").val()) {
            flag = false;
        } else {
            age = $("#age").val();
            if(age<0){
                flag = false;
                message="Veuillez entrez un age valide"
            }
        }
        persCharg = parseInt($("#personnesAcharges").val());

        if (($("#femme").prop("checked") === true)) {
            genreInfo = "Femme";
        }
        if (flag) {
            table.append("<tr><td>" + nom + "</td><td>" + prenom + "</td><td>" + genreInfo + "</td><td>" +
                age + "</td><td>" + telephone + "</td><td>" + persCharg + " personnes </td></tr>");
        }
        else {
            alert(message);
        }
    }

);
