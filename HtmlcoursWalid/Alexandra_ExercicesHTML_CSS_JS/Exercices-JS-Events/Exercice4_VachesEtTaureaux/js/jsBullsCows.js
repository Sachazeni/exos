
var succes = "Congratulations! You've found the secret number on "

var coups = 0;
var enCours = false;
var secret;
var choixLongueur;
var difficulteChoisie;
var succes = "Congratulations! You've found the secret number on "

/**
 * Methode pour generer un tableau de nombres aleatoirement
 * @param : a difficulté pour definir la taille du nombre
 * @returns : un tableau avec des chiffres remplis aleatoirement
 */
function nombreAleatoire(difficulte) {
    var nb = [];
    while (nb.length < +difficulte) {
        //generation nb aleatoire 
        var r = Math.floor(Math.random() * 9) + 1;
        if (nb.indexOf(r) === -1) nb.push(r);
    }
    return nb;
};



/**
 * Methode pour commencer un nouveau jeu
 */
function newGame() {
    coups = 0;
    $("#userInput").val("");
    coups = 0;
    difficulteChoisie = $("input[name='difficulte']:checked").val();
    secret = nombreAleatoire(difficulteChoisie);
    $("#hideThis").css("display", "block");
    $("#historique").val("You have started a new game ! the difficulty is : " + difficulteChoisie + " Good luck :)");
}



/**
 *Methode pour afficher la partie du jeu
 */
$("#playBtn").click(function () {
    $("#contenantJeu").css("visibility", "visible");
    $(".3").prop("checked", true);
    secret = nombreAleatoire(3);
});


$("#tryBtn").click(function () {
    var bulls = 0;
    var cows = 0;


    if ($("#userInput").val()) {
        var recupUserInput = $("#userInput").val();
        var oldTry = $("#historique").val();
        if (recupUserInput.length === secret.length) {
            coups++;
            enCours = true;
            var temp = recupUserInput.split('');
            var userInput = temp.map(function (x) {
                return parseInt(x, 10);
            });

            for (var i = 0; i < userInput.length; i++) {
                if (secret.includes(userInput[i])) {
                    cows++;
                    if (userInput[i] === secret[i]) {
                        bulls++;
                        cows--;
                    }
                }
            }
            if (bulls === +difficulteChoisie) {
                $("#historique").val(succes + coups + " try! \n" + oldTry);
            }
            else {
                $("#historique").val("Entry : " + recupUserInput + " : You've found " + bulls + " bulls and " + cows + " cows !" + "\n______________________________________________________________\n" + oldTry);
            }



        }
        else {
            alert("Invalid Input, You have selected the difficulty with " + difficulteChoisie + " numbers !");
        }

    }
    else {
        alert("Empty input!");
    }


})

$("#radioBtnDifficulty").change(function () {
    newGame;
});

/**
 * Methode pour lancer une nouvelle partie
 */
$("#newGameBtn").click(newGame);

/**
 * Methode  pour le bouton Clear
 */
$("#clearBtn").click(function () {
    $("#historique").val("");
    $("#userInput").val("");
});


