$("#num").focus(function () {
  $("#res").css("background-color", "white");
});
/* pas supprimer*/
var res = 0;

$("#plus").click(
  function (event) {
    numEntree = parseInt($("#num").val());
    res += numEntree;
    $("#res").val(res);
  }
);

$("#moins").click(
  function (event) {

    numEntree = parseInt($("#num").val());
    res -= numEntree;

    $("#res").val(res);
  }
);

$("#divis").click(
  function (event) {
    numEntree = parseInt($("#num").val());
    if (numEntree === 0) {
      $("#res").val("Pas de division par 0 !");
      $("#res").css("background-color", "pink");
      $("#efface").css("display", "initial");
      $("#groupe").css("display", "none");
    }
    else {
      numEntree = parseInt($("#num").val());
      res /= numEntree;
      $("#res").val(res);
    }
  }
);

$("#efface").click(
  function (event) {
    $("#num").val("0");
    $("#res").val("0");
    $("#efface").css("display", "none");
    $("#groupe").css("display", "initial");
    $("#res").css("background-color", "white");
  }
)
