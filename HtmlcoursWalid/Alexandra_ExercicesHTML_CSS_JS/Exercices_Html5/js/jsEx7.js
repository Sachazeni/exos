function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}
var compteurTentatives = 0;
var nombreAle = getRandomInt(100);

var appelFonctionPrincip = function (event) {

  if ($.isNumeric($("#choixUt").val())) {
    var nbUtilisateur = parseInt($("#choixUt").val());
    console.log(nombreAle);

    if (nbUtilisateur === nombreAle) {
      $("#hid").val(nombreAle)
      $("#res").css("display", "initial");
      $("#valid").css("display","none");
      $("#indices").val("Trouvé ! Vous avez trouvé le nombre en " + compteurTentatives + " tentatives !");
    }
    else if (nbUtilisateur > nombreAle) {
      compteurTentatives += 1;
      $("#tentatives").val(compteurTentatives);
      $("#indices").val("Trop grand !");

    }

    else if (nbUtilisateur < nombreAle) {
      compteurTentatives += 1;
      $("#tentatives").val(compteurTentatives);
      $("#indices").val("Trop petit !");
    }
  }
  else {
    $("#indices").val("Aucune valeur entrée :(");
  }
};

$("#valid").click(appelFonctionPrincip);
$("#choixUt").keypress(function () {
  if (event.which == 13) appelFonctionPrincip();
}
);

$("#res").click(function () {
  nombreAle = getRandomInt(100);
  compteurTentatives=0;
  $("#hid").val("?");
  $(this).css("display","none");
});