
var etat = 0;
var temps = 0;
var interval;

//fonction pour start
function debut(event) {

  etat = 1;// timer déjà lancé (0 sinon)

  //desactiver le bouton start pour pas que l'incrementation puisse augmenter

  $("#start").attr("disabled", true);

  //appel la fonction du compteur dans un interval de 100
  interval = setInterval(compteur, 100)
  $("#info").css("visibility", "hidden");
  $("#ens").css("background-color","rgb(127, 183, 216)");
}

//fonction pour stop
function arret(event) {
  etat = 0;//timer stoppé
  $("#start").attr("disabled", false);
  clearInterval(interval);
  $("#info").css("visibility", "visible");
  $("#ens").css("background-color","#b2b8ca")

}

//fonction pour reset
function clear(event) {
  etat = 0;//timer stoppé
  temps = 0;
  //reboot de interval
  clearInterval(interval);
  $("#start").attr("disabled", false);
  $("#chronometreAffiche").val("00 h : 00 min : 00 s : 0 ms");
  $("#info").css("visibility", "hidden");
  $("#ens").css("background-color","rgb(127, 183, 216)");

}
//fonction compteur qui va s'occuper de l'affichage
function compteur(event) {
  if (etat == 1) {

    //temps va s'incrementer de 100 à chaque intervalle de 100ms
    temps += 100;

    // variable laquelle on va diviser pour recuperer les minutes secondes etc...
    var temporellePourCalcul = temps;

    //calcul des variables secondes, minutes etc
    //on recup le reste de la division pout les milisecondes
    var mSec = Math.floor(temporellePourCalcul % 1000);

    //division par 1000 pour recup le temps en sec
    temporellePourCalcul = Math.floor(temporellePourCalcul / 1000);

    //calcul des secondes en recuperant le reste de la division pour les sec
    var sec = Math.floor(temporellePourCalcul % 60);

    //division pour recuperer le temps en min
    temporellePourCalcul = Math.floor(temporellePourCalcul / 60);

    //modulo pour recuperer le reste de la division par 60=les minutes
    var minutes = Math.floor(temporellePourCalcul % 60);

    //redivision par 60 pour avoir les heures 
    temporellePourCalcul = Math.floor(temporellePourCalcul / 60);

    var heures = Math.floor(temporellePourCalcul);


    if (heures<10) {
      heures = "0" + heures;
    }
    if (minutes < 10) {
      minutes = "0" + minutes;
    }
    //si les seocndes sont sup à 60 on applique le modulo pour recuperer la bonne valeur des secondes
    if (sec < 10) {
      sec = "0" + sec;
    }
 

    $("#chronometreAffiche").val(heures + " h : " + minutes + " min : " + sec + " s : " + mSec/100+" ms");
  }
}
$("#start").click(debut);
$("#stop").click(arret);
$("#res").click(clear);

