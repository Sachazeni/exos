
var date = new Date();

function afficher(event) {
  var jour = parseInt(date.getDay());
  var afficherJour;

  if (jour === 0) {
    afficherJour = "Dimanche";
    $("#emplacmentJour").css("color","#ff6600");
  }
  else if (jour === 1) {
    afficherJour = "Lundi";
    $("#emplacmentJour").css("color","#3333cc");
  }
  else if (jour === 2) {
    afficherJour = "Mardi";
    $("#emplacmentJour").css("color","#cc0000");
  }
  else if (jour === 3) {
    afficherJour = "Mercredi";
    $("#emplacmentJour").css("color","#008000");
  }
  else if (jour === 4) {
    afficherJour = "Jeudi";
    $("#emplacmentJour").css("color","#ffd11a");
  }
  else if (jour === 5) {
    afficherJour = "Vendredi";
    $("#emplacmentJour").css("color","##ff99cc");
  }
  else if (jour === 6) {
    afficherJour = "Samedi";
    $("#emplacmentJour").css("color","#33001a")
  }
  $("#emplacmentJour").html( afficherJour);
}

$("#afficherJour").click(afficher);
