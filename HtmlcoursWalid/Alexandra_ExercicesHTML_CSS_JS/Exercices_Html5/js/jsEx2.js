

/* Changer l"emplacement de l'image via le click*/
$("#gauche").click(
    function (event) {
        $("#carreau").css("left", $("#carreau").position().left - 50);
    }
);
$("#droite").click(
    function (event) {
        $("#carreau").css("left", $("#carreau").position().left + 50);
    }
);
$("#haut").click(
    function (event) {
        $("#carreau").css("top", $("#carreau").position().top - 50);
    }
);
$("#bas").click(
    function (event) {
        $("#carreau").css("top", $("#carreau").position().top + 50);
    }
);

/* Changer l"emplacement de l'image via le clavier*/
$(document).keydown(
    function (event) {
        /* si le code correspond a fleche droite*/
        if (event.which == '37') {
            $("#carreau").css("left", $("#carreau").position().left - 50);
        }
        /* si le code correspond a fleche haut*/
        if (event.which == '38') {
            $("#carreau").css("top", $("#carreau").position().top - 50);
        }
        /* si le code correspond a fleche droite*/
        if (event.which == '39') {
            $("#carreau").css("left", $("#carreau").position().left + 50);
        }
        /* si le code correspond a fleche bas*/
        if (event.which == '40') {
            $("#carreau").css("top", $("#carreau").position().top + 50);
        }
    }
);

