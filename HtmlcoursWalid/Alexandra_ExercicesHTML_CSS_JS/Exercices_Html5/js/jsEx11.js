
var tbody = $('#myTable').children('tbody');
var table = tbody.length ? tbody : $('#myTable');

$("#multBtn").click(function(){
  var nombreUtilisateur=$("#nombreChoisi").val();
  
  //test si le nombre est un entier, operateur + pour transformer la chaine de caractere
// en numeric
if(nombreUtilisateur.length>0){
  if(Number.isInteger(+nombreUtilisateur)){
    $("#myTable").css("display","initial");
    $("#msgErreur").css("display","none");
    multiplier(nombreUtilisateur);
  }
  else{
    $("#msgErreur").html("La valeur entrée n'est pas un nombre entier!");
    $("#msgErreur").css("display","initial");
    $("#myTable").css("display","none");
    
    
  }
}
else{
  $("#msgErreur").html("Veuillez saisir un nombre entier!");
  $("#msgErreur").css("display","initial");
  
}

});

function multiplier(nombre){
  var i ;
  var resultat;
for (i= 0; i <= 10; i++) {
  resultat=i*nombre;
  table.append("<tr><td>" + nombre + "</td><td>" + i + "</td><td>" + resultat + "</td><td></tr>");
}
};
