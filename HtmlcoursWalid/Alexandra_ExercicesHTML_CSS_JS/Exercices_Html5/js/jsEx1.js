
/* Changer la couelur du fond de l'image  via la liste deroulante*/
$("#list-color").change(function () {
    $("#vectorImg").css("background-color", $(this).val());
});

/* Changer la couelur du fond de l'image via la palette*/
$("#palette").change(function () {
    $("#vectorImg").css("background-color", $(this).val());
});

/* Changer la taille de l'image via le bouton*/
$("#boutonTaille").click(
    function (event) {
        $("#vectorImg").css("height", $("#hauteur").val());
        $("#vectorImg").css("width", $("#largeur").val());
    }
);

/* Changer la taille de l'image en etant sur les deux champs et en pressant entrée*/
$("#hauteur,#largeur").keypress(function (event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
        $("#vectorImg").css("height", $("#hauteur").val());
        $("#vectorImg").css("width", $("#largeur").val());
    }
});

/* Changer l"emplacement de l'image via le bouton*/
$("#boutonPosition").click(
    function (event) {
        $("#vectorImg").css("margin-left", $("#horiz").val()+"px");
        $("#vectorImg").css("margin-top", $("#vert").val()+"px");
    }
);

/* Changer l"emplacement de l'image appuyant sur entrée*/
$("#horiz,#vert").keypress(
    function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
        $("#vectorImg").css("margin-left", $("#horiz").val()+"px");
        $("#vectorImg").css("margin-top", $("#vert").val()+"px");
        }
    }
);

/* Changer l'image appuyant sur un des boutons sous l'image avec les id correspondants*/
$("#coeur").click(
    function (event) {
        $("#vectorImg").attr("src", "./img/zodiac-drawing-art-deco-3.png");
    }
);

$("#trefle").click(
    function (event) {
        $("#vectorImg").attr("src", "./img/alfons-mucha-2619140_960_720.png");
    }
);
$("#cat").click(
    function (event) {
        $("#vectorImg").attr("src", "./img/cat-logo-E6BC30BEB2-seeklogo.com.png");
    }
);

$("#diams").click(
    function (event) {
        $("#vectorImg").attr("src", "./img/622d5f83926ddaca62df9951e5464dda.png");
    }
);