testFonction=function(){
  var canvas = document.getElementById("tstCanvas");
  var c=canvas.getContext("2d");
  var image=document.getElementById("imgBase");
  /* remplit le canvas avec l'image*/
  c.drawImage(image,0,0);

  /* Ddefinit la couleur du dessin*/
  c.fillStyle="black";

  /* Definit l'emplacement du carrée*/
  c.fillRect(490,155,80,80);

  c.strokeStyle="white";
  c.scale(1, 0.5);
  c.beginPath();
  c.lineWidth="3";
  c.arc(120,350,20,0,2*Math.PI);
  c.stroke();

}();
