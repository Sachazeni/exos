var salBrut = 0;
var persCharg = 0;
var reduction = 0;
var salNet = 0;
var tbody = $('#myTable').children('tbody');
var table = tbody.length ? tbody : $('#myTable');

$("#radioButtonRevenusContainer input:radio").click(function () {
  if ($(this).val() === "1") {
    $("#allMontant").css("display", "initial");
    $("#bonusM").css("display", "none");
  }
  else if ($(this).val() === "2") {
    $("#bonusM").css("display", "initial");
    $("#allMontant").css("display", "none");
  }
  else if ($(this).val() === "3") {
    $("#bonusM").css("display", "none");
    $("#allMontant").css("display", "none");
  }
});


$("#calcul").click(
  function (event) {
    if (!$("#salB").val()) {
      alert("Le champ pour le Salaire Brut est vide!")
    } else {
      salBrut = parseFloat($("#salB").val());
      if(salBrut>0){
      persCharg = parseInt($("#personnesAcharges").val());
      var impot = 18;
      var reductionPourInfo = 0;
      var revEnPlus = 0;
      var genreInfo = "Homme";

      if ($("#alloc").prop("checked") === true & $.isNumeric($("#allMontant").val())) {
        revEnPlus = parseFloat($("#allMontant").val());
      }
      else if ($("#bonus").prop("checked") === true & $.isNumeric($("#bonusM").val())) {

        revEnPlus = parseFloat($("#bonusM").val());
      }
      if (($("#femme").prop("checked") === true)) {
        impot -= 2;
        reductionPourInfo += 2;
        genreInfo = "Femme";
      }
      if (persCharg === 3) {
        impot -= 1;
      }
      else if (persCharg === 4) {
        impot -= 2;
        reductionPourInfo += 2;
      }

      salNet = revEnPlus + (salBrut - ((salBrut * impot / 100) + (salBrut * 7 / 100) + (salBrut * 5 / 100)));

      table.append("<tr><td>" + genreInfo + "</td><td>" + salBrut + "</td><td>" + parseFloat(salNet).toFixed(2) + "</td><td>" +
        revEnPlus + "</td><td> -" + reductionPourInfo + "%</td><td>" + impot + "%</td><td>" + persCharg + "</td></tr>");
    }
      alert("Le salaire brut ne peut être negtif")
    }

  }
);
