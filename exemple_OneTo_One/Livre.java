package test.onetoone;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;



@Entity
@Table(name="Livre")
public class Livre {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="idBook")
	private int idBook;
	@Column(name="auteur")
	private String auteur;
	@Column(name="titre")
	private String titre;
	@Column(name="prix")
	private Integer prix;
	@Column(name="isbn")
	private String isbn;
	@Column
	private Date date;
	
	/*@Column
	private Map<Integer,String> listeChaines;
	*/
	@Column
	private LocalDate localDate;
	
	@Column
	private LocalDateTime time;
	
	@OneToOne(mappedBy="livre")
	private Page page;
	
	/**
	 * @return the localDate
	 */
	public LocalDate getLocalDate() {
		return localDate;
	}





	/**
	 * @param localDate the localDate to set
	 */
	public void setLocalDate(LocalDate localDate) {
		this.localDate = localDate;
	}





	


	public Livre( String auteur, String titre, Integer prix, String isbn, Date date, LocalDateTime time,LocalDate localDate) {
		super();

		this.auteur = auteur;
		this.titre = titre;
		this.prix = prix;
		this.isbn = isbn;
		this.date = date;
		this.time = time;
		this.localDate = localDate;


	}



	

	/**
	 * @return the time
	 */
	public LocalDateTime getTime() {
		return time;
	}





	/**
	 * @param time the time to set
	 */
	public void setTime(LocalDateTime time) {
		this.time = time;
	}





	public Date getDate() {
		return date;
	}







	public void setDate(Date date) {
		this.date = date;
	}







	/**
	 * 
	 * @param idBook
	 * @param auteur
	 * @param titre
	 * @param prix
	 * @param isbn
	 */
	public Livre(int idBook, String auteur, String titre, Integer prix,String isbn) {

		this.idBook = idBook;
		this.auteur = auteur;
		this.titre = titre;
		this.prix = prix;
		this.isbn = isbn;

	}

	public Livre() {

		this.auteur = "";
		this.titre = "";
		this.prix =0;
		this.isbn = "";
	}

	/**
	 * 
	 * @param auteur
	 * @param titre
	 * @param prix
	 * @param isbn
	 */
	public Livre(String auteur, String titre,  String isbn, Integer prix) {

		this.auteur = auteur;
		this.titre = titre;
		this.prix = prix;
		this.isbn = isbn;
		page = new Page();

	}


	/**
	 * @return the idBook
	 */
	public int getIdBook() {
		return idBook;
	}
	/**
	 * @param idBook the idBook to set
	 */
	public void setIdBook(int idBook) {
		this.idBook = idBook;
	}
	/**
	 * @return the auteur
	 */
	public String getAuteur() {
		return auteur;
	}
	/**
	 * @param auteur the auteur to set
	 */
	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}
	/**
	 * @return the titre
	 */
	public String getTitre() {
		return titre;
	}
	/**
	 * @param titre the titre to set
	 */
	public void setTitre(String titre) {
		this.titre = titre;
	}
	/**
	 * @return the prix
	 */
	public Integer getPrix() {
		return prix;
	}
	/**
	 * @param prix the prix to set
	 */
	public void setPrix(Integer prix) {
		this.prix = prix;
	}
	/**
	 * @return the isbn
	 */
	public String getIsbn() {
		return isbn;
	}
	/**
	 * @param isbn the isbn to set
	 */
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	/*@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((auteur == null) ? 0 : auteur.hashCode());
		result = prime * result + idBook;
		result = prime * result + ((isbn == null) ? 0 : isbn.hashCode());
		result = prime * result + ((prix == null) ? 0 : prix.hashCode());
		result = prime * result + ((titre == null) ? 0 : titre.hashCode());
		return result;
	}
	 (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (auteur == null) {
			if (other.auteur != null)
				return false;
		} else if (!auteur.equals(other.auteur))
			return false;
		if (idBook != other.idBook)
			return false;
		if (isbn == null) {
			if (other.isbn != null)
				return false;
		} else if (!isbn.equals(other.isbn))
			return false;
		if (prix == null) {
			if (other.prix != null)
				return false;
		} else if (!prix.equals(other.prix))
			return false;
		if (titre == null) {
			if (other.titre != null)
				return false;
		} else if (!titre.equals(other.titre))
			return false;
		return true;
	}*/

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	
	/**
	 * @return the pageList
	 */
	public Page getPage() {
		return page;
	}

	/**
	 * @param pageList the pageList to set
	 */
	public void setPage(Page page) {
		this.page = page;
	}





	





	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Livre [idBook=" + idBook + ", auteur=" + auteur + ", titre=" + titre + ", prix=" + prix + ", isbn="
				+ isbn + ", date=" + date + ", time=" + time + ", page=" + page + "]";
	}






}
