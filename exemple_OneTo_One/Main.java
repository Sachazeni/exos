package test.onetoone;


import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.cortex2i.utils.HibernateUtils;
import test.onetoone.Livre;
import test.onetoone.Page;

public class Main {
	private static Session s = null;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// Ouverture d'une session Hibernate
		s = HibernateUtils.getSession();
				
		// D�but de la transaction
		Transaction tx = s.beginTransaction();
		
		// Instanciation d'objet
		Livre monPremierLivre = new Livre("auteur1", "titre1", 12, "isbn1",Date.valueOf(LocalDate.now()),LocalDateTime.now(),LocalDate.now());
				
				//b.setTime(LocalDateTime.now());
				
				System.out.println("Timestamp.valueOf(LocalDateTime.now())  =+> " + Timestamp.valueOf(LocalDateTime.now()));
				
				System.out.println(monPremierLivre);
				
				
				Livre monDeuxiemeLivre = new Livre("auteur3", "titre3", "isbn3", 17);
				
				Page p = new Page("contenu22", 5);
				Page p1 = new Page("contenu11", 10);
				
				
				p.setLivre(monPremierLivre);
				p1.setLivre(monDeuxiemeLivre);
				
				monPremierLivre.setPage(p);
				monDeuxiemeLivre.setPage(p1);
				
				s.save(p);
				//b.setPage(p);
				
               s.save(p1);
				
				
				// Enregistrements
				
				s.save(monPremierLivre);
				
			//	s.save(monDeuxiemeLivre);
				
				s.save(monDeuxiemeLivre);
				
				
				
				
				tx.commit();

				// Lister les livres
				listerLivres();
				// Fermeture de la session Hibernate
				s.close();
				
	}
	
	
	// Affiche le contenu de la table USERS
		
		private static void listerLivres() {
			System.out.println("---Book---");
			// Cr�ation de la requ�te
			Query q = s.createQuery("from Livre");
			ArrayList<Livre> list = (ArrayList<Livre>) q.list();
			// Affichage de la liste de r�sultats
			for (Livre b: list) {
						System.out.println("Book ==> [idBook] =" + b.getIdBook()  + "\t" +
						"[auteur] = " + b.getAuteur()  + "\t" +
						"[titre ] = "  + b.getTitre()  + "\t" +
						"[prix ] = " + b.getPrix()  + "\t" +
						"[isbn ] = " + b.getIsbn() + "]" +
						"[Time ] = " + b.getTime() + "]" +
						"[Local DATE ] = " + b.getLocalDate() + "]" +
						" ==> Identifiant de la page : "+b.getPage().getIdPage()  + 
						" ==> Contenu de la page : "+b.getPage().getContenu() + 
						" ==> Identifiant de la page : "+b.getPage().getNumPage()
						
							);
			}
			
			
			list.forEach(livre->{
				if(livre != null) {
					System.out.println(livre);
				}
			});
			
			
			
		}

}
