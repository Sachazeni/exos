package test.onetoone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.*;
import javax.persistence.Table;

@Entity
@Table
public class Page {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column
	private int idPage;
	@Column
	private String contenu;
	@Column
	private int numPage;
	@OneToOne
	  @JoinColumn(name="idBook") 
	private Livre livre;
	
	
	/**
	 * @return the idPage
	 */
	public int getIdPage() {
		return idPage;
	}
	/**
	 * @param idPage the idPage to set
	 */
	public void setIdPage(int idPage) {
		this.idPage = idPage;
	}
	/**
	 * @return the contenu
	 */
	public String getContenu() {
		return contenu;
	}
	/**
	 * @param contenu the contenu to set
	 */
	public void setContenu(String contenu) {
		this.contenu = contenu;
	}
	/**
	 * @return the numPage
	 */
	public int getNumPage() {
		return numPage;
	}
	/**
	 * @param numPage the numPage to set
	 */
	public void setNumPage(int numPage) {
		this.numPage = numPage;
	}
	public Page(String contenu, int numPage) {
		this.contenu = contenu;
		this.numPage = numPage;
	}
	
	public Page() {
		this.contenu = "";
		this.numPage = 0;
		
	}

	
	public void setLivre(Livre livre) {
		this.livre = livre;
	}
	
	public Livre getLivre() {
		return this.livre ;
	}

}
