//recup du bouton pour afficher
let getBoutonAfficher=document.getElementById("aff");

//recup du bouton pour cacher
let getBoutonCacher=document.getElementById("cach");

//recup du texte à survoler
let getTexteSurvol=document.getElementById("surv");


//si bouton afficher est cliqué : ajout event
if(getBoutonAfficher){
    getBoutonAfficher.addEventListener("click",afficheText);
}

//si le bouton caché est cliqué: ajout event
if(getBoutonCacher){
    getBoutonCacher.addEventListener("click",hiddeText);
}

//quand le texte specifique est survollé ou la souris quitte le texte
if(getTexteSurvol){
getTexteSurvol.addEventListener("mouseover", modifyTxt);
getTexteSurvol.addEventListener("mouseleave", hiddeText);
}


//Les fonctions-------------------------------------------------------------------------------
//afficher
function afficheText(){
document.getElementById("postIt").style.visibility='visible';
document.getElementById("postIt").innerHTML="Vous avez cliqué sur le bouton 'Afficher'";
}


//cacher
function hiddeText(){
    document.getElementById("postIt").style.visibility='hidden';
   
    }

//modifier lors du survol
function modifyTxt(){
    
    document.getElementById("postIt").style.visibility='visible';
    document.getElementById("postIt").innerHTML="C'est gentil de me survoler...";
}


