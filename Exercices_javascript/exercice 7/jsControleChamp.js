//methode pour test le bouton DOM-0
document.getElementById("btnContr2").onclick = controle;

//recup bouton pour test event
var getBoutonPourEvent =document.getElementById("btnContr3");


//ajout event au bouton 3
getBoutonPourEvent.addEventListener("click", controle);



//la fonction appele pour les trois boutons.
function controle(){

    var recupTexte =document.getElementById("znTxt").value;
    var regex= new RegExp("[a-zA-Z]{2,}");

    if(!regex.test(recupTexte)){
    alert("La chaine doit comporter au moins 2 caractères!");
    }
    else{
        alert("Vous avez saisi : "+recupTexte);
    }

}

