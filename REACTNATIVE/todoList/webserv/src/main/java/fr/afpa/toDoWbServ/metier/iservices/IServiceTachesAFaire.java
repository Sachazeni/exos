package fr.afpa.toDoWbServ.metier.iservices;

import fr.afpa.toDoWbServ.metier.entities.TachesAFaire;

public interface IServiceTachesAFaire {
	
public TachesAFaire creationTache(TachesAFaire tache);
public TachesAFaire deleteTache(TachesAFaire tache);
public TachesAFaire faite(TachesAFaire tache);

}
