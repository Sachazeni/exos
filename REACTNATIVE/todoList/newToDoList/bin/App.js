import 'react-native-gesture-handler';
import React from "react";

import List from './List.js';
import Acceuil from './Acceuil.js';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

export default function App() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
        <Stack.Screen name="Accueil" component={Acceuil} />
        <Stack.Screen name="List" component={List} />
        
        </Stack.Navigator>
    </NavigationContainer>
      
    );
  }
