import React, {useState} from 'react';
import {Text, View, TextInput, StyleSheet, Button, } from 'react-native';

export default function ajouter({submitHandler}) {
const [text, setText]=useState('');
const changeHandler =(valeur)=>{
    setText(valeur);
}
return(
    <View>
        <TextInput
        style= {styles.input}
        placeholder="ajouter ..."
        onChangeText={changeHandler}/>
        <Button onPress={()=>submitHandler(text)} title='ajouter' color='tomato'/>
    </View>
)
}
const styles = StyleSheet.create({
    input: {
        marginBottom: 10,
        paddingHorizontal:8,
        paddingVertical:6,
        borderBottomWidth:1,
        borderBottomColor: 'tomato'
    }
})