

import React from 'react';
import { TouchableOpacity, Text, StyleSheet, View } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';

export default function TodoItem({ item, pressHandler }) {
    return (
        <TouchableOpacity onPress={() => pressHandler(item.key)}>
            <View style={styles.item}>
                <MaterialCommunityIcons name='delete-outline' size={20} color='tomato' />
                <Text style={styles.decoTxt}>{item.text}</Text>
            </View>

        </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
    item: {
        flexDirection: 'row',
        padding: 16,
        marginTop: 16,
        borderColor: 'tomato',
        borderWidth: 1,
        borderStyle: 'dashed',
        borderRadius: 10
    },
    decoTxt: {
        marginLeft: 10,
    }


})