
import React, { useState } from 'react';
import {Text, View, StyleSheet, FlatList, Alert, TouchableWithoutFeedback, Keyboard } from 'react-native';
import imagesStylo from './assets/images/john-jennings-Aet6IBKXJSg-unsplash.jpg';
import Header from './components/Header.js';
import TodoItem from './components/toDoItem.js';
import Ajouter from './components/ajouterToDo.js';


export default function List() {

  const [todos, setTodos] = useState([
]);

const pressHandler=(key)=> {
  setTodos((prevTodos)=>{
    return prevTodos.filter(todo=>todo.key !=key);
  });
}
const submitHandler =(text)=>{
  if(text.length>2){
    setTodos((prevTodos)=>{
      return [
        {text:text, key:Math.random().toString()},
        ...prevTodos];
    });
  }
  else{
    Alert.alert('Error','Ajouter plus de deux lettres'),[
      {
        text:"ok", onPress: () => console.log("alert ferme")
      }
    ]
  }
  
}
  return (
    <TouchableWithoutFeedback on press={()=>{
      Keyboard.dismiss();
    }}>
    <View style={styles.container}>
      {/*Entête */}
      <Header/>
      <View style={styles.contenu}>
        {/* Forme Todo */}
        <Ajouter submitHandler={submitHandler} />
        <View style={styles.list}>
          <FlatList
            data={todos}
            renderItem={({item}) => (
             <TodoItem item={item} pressHandler={pressHandler}/>
            )}
          />

        </View>
      </View>
    </View>
    </TouchableWithoutFeedback>
  );
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
   contenu:{
     padding: 40,
     flex:1,
   },
   list: {
     flex: 1,
     marginTop: 20,
   }
});