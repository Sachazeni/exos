
import * as React from "react";
import imagesLivres from './assets/images/thomas-kelley-hHL08lF7Ikc-unsplash.jpg';

import { Platform, StyleSheet, Text, View, ImageBackground,TouchableOpacity} from 'react-native';



export default function Acceuil({navigation}){
   
      return (
        
<View style={styles.container}>
          <ImageBackground source={imagesLivres} style={styles.imageBack}>
          <TouchableOpacity onPress={() => navigation.navigate('List')} style={styles.button}>
          <Text style={styles.buttonText}>My to-do List</Text>
        </TouchableOpacity>
        </ImageBackground>
        </View>
      );
    }
 
  
const styles = StyleSheet.create({
    imageBack: {
      width: '100%',
      height: '100%',
      justifyContent: 'center',
      alignItems: 'center',
    },
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#AAAAAA',
    },
    textDeco: {
      fontSize: 21,
      justifyContent: 'center',
      textAlign: 'center',
      color:'#ffffff', 
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
    button: {
      backgroundColor: "#ffffff",
    
      padding: 20,
      textAlign: 'center',
      fontFamily: 'Merienda',
      borderRadius: 5,
      width: '40%',
      height: '10%',
      borderRadius: 25,
    },
    buttonText: {
      fontSize: 20,
      color:'#2e1b00', 
    }, 
  });