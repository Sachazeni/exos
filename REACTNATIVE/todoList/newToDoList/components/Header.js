
import React from 'react';
import {Text, View, StyleSheet } from 'react-native';


export default function Header() {
return(
    <View style={styles.header}>
        <Text style={styles.titre}>My ToDos</Text>
    </View>
)
}
const styles = StyleSheet.create({
    header: {
        height:80,
        padding: 38,
        backgroundColor: 'tomato',
        justifyContent: 'center',
    },
    titre:{
        textAlign:"center",
        color: '#fff',
        fontSize:20,
        fontWeight: "bold",
        fontFamily:'Roboto',

    }

});