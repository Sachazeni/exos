package fr.afpa.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ServeletListe
 */
public class ServeletListe extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServeletListe() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//tjr ajouter jsp ou html quand c'est une page jsp ou html. 
		//attention au / a pas mettre
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("index.jsp");
		
		List<String>list =new ArrayList<String>();
		list.add("Hello");
		list.add("World");
		list.add("of");
		list.add("Cats");
		request.setAttribute("listeCats", list);
		
		//on met lise dans la requete
		//pour pouvoir appeler la page avec la list il faut surtout appeler la servlet pour qu'elle renvoie une liste complete.
		
		requestDispatcher.forward(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
