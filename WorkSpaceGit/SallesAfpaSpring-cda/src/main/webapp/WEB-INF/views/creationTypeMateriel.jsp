<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
  <%@page import="fr.afpa.salleafpa.outils.Parametrage"%>
<%@page import="org.omg.CORBA.portable.ApplicationException"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cr�ation Salle</title>
<link href="${pageContext.request.contextPath}/resources/css/menu.css" rel="stylesheet">
<link href ="http://localhost:8080/gestionsalleafpa2/css/cssCreationSalle.css" rel="stylesheet">
</head>
<body>
	<c:choose>
		<c:when
			test="${sessionScope.persAuthSalle.role.id eq Parametrage.ADMIN_ID }">
			
			<jsp:include page="menu.jsp" />

        <div class="container">
            
                <h2>Creation d'un nouveau type materiel</h2>         
            <div class="ligne">
            
            <!--Debut de FORM --> 
                <form action = "${Parametrage.URI_AJOUTER_TYPE_MATERIEL}"method="post">
                
                    <!--COLONNE GAUCHE-->
                    <div class="colonne">
                    
                        <!--partie nom-->
                        <div class="colonneGauche"> 
                            <label>Type Materiel :</label>
                        </div>
                        <div class="colonneDroite"> 
                            <input name="Type Materiel" value="${nom}" type="text" required="">
                             <br>
                            <em>${nomKO }</em>
                        </div>
                        
                       
                        <!--partie BOUTON VALIDER-->
                        <div class="colonneGauche"> 
                            <label></label>
                        </div>
                        <div class="colonneDroite"> 
                            <button type="submit" value="Valider">Valider</button>
                        </div>
                        
                        
                        <!--partie BOUTTON ANNULER retour vers l'accueil � integrer-->
                        <div class="colonneGauche"> 
                            <label></label>
                        </div>
                        <div class="colonneDroite"> 
                            <button type="reset" value="Annuler">Annuler</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
   
		</c:when>
		<c:otherwise>
			<c:redirect url=""></c:redirect>
		</c:otherwise>
	</c:choose>
</body>
</html>