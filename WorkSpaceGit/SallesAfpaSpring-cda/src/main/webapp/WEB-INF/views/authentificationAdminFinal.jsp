<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"  %>
    <%@page import="fr.afpa.salleafpa.outils.Parametrage"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>admin</title>
<link href ="${pageContext.request.contextPath}/resources/css/cssAuthentification.css" rel="stylesheet">
</head>
<body>
<flex-container>
            <label id="labelInfo">Connexion Administrateur</label>
            
            <em class="erreur"> ${KO}</em>
         
            
            <form action="${Parametrage.URI_AUTHENTIFICATION_ADMIN }" method="post">
                <label for="LoginLabel">Login :</label>
                <br> 
                <input name="loginAdmin" type="text" required/> 
                <br> 
                <br> 
                <br> 
                <label for="MdpLabel">Mot de passe :</label>
                <br> 
                <input name="mdpAdmin" type="password" required/> 
                <br> 
                <br> 
                <br> 
                <button type="submit" value="ValiderB" >Valider</button>
            </form>
 </flex-container>
</body>
</html>