package fr.afpa.salleafpa.metier.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.salleafpa.dto.iservices.IServiceBatimentDto;
import fr.afpa.salleafpa.metier.entities.Batiment;
import fr.afpa.salleafpa.metier.iservices.IServiceBatiment;

@Service
public class ServiceBatiment implements IServiceBatiment {

	@Autowired
	private IServiceBatimentDto servBatimentDto;
	
	@Override
	public List<Batiment> getAll() {
		return servBatimentDto.getAll();
	}

	@Override
	public boolean saveBatiment(String nom) {
		
	Batiment batiment = new Batiment();
	
	batiment.setNom(nom);
	return servBatimentDto.saveBatiment(batiment);

	}

	@Override
	public boolean modifBatiment(String nom) {
		
		Batiment batiment = new Batiment();
		
		batiment.setNom(nom);
		
		return servBatimentDto.modifBatiment(batiment);
	}

	@Override
	public Batiment getOneBatiment(int id) {
		return servBatimentDto.getOneBatiment(id);
	}
}
