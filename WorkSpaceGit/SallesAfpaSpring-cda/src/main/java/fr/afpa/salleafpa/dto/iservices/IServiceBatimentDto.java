package fr.afpa.salleafpa.dto.iservices;

import java.util.List;

import fr.afpa.salleafpa.metier.entities.Batiment;

public interface IServiceBatimentDto {
	
	public List<Batiment> getAll();

	public boolean saveBatiment(Batiment batiment);

	public boolean modifBatiment(Batiment batiment);

	public Batiment getOneBatiment(int id);
}