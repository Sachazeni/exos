package fr.afpa.salleafpa.outils;

public class Parametrage {
	public static final int ADMIN_ID= 1;
	public static final int UTILISATEUR_ID= 2;
	
	public final static int ID_FONCTION_DIRECTEUR=1;
	public final static int ID_FONCTION_FORMATTEUR=2;
	public final static int ID_FONCTION_STAGIAIRE=3;
	public final static int ID_FONCTION_SECRETAIRE=4;
	public final static int ID_FONCTION_VISITEUR=5;
	public final static int ID_FONCTION_INTERVENANT=6;
	
	public static final int NB_RESERV_PAGE=8;
	
	public static final String URI_AUTHENTIFICATION_ADMIN= "admin";
	public static final String URI_DECONNEXION = "/salleafpa/deconnexion";
	public static final String URI_MODIFICATION = "/salleafpa/admin/accueil/recherche/modifier";
	public static final String URI_ACCUEIL = "/salleafpa/admin/accueil";
	public static final String URI_NEW_USER = "/salleafpa/admin/accueil/creation";
	public static final String URI_RECHERCHE = "/salleafpa/admin/accueil/recherche";
	public static final String URI_AUTHENTIFICATION = "/salleafpa";
	
	
	public static final String URI_VISUALISER_SALLE = "/salleafpa/visualiser";
	public static final String URI_MODIFIER_SALLE = "/salleafpa/visualiser/salle/modifier";
	public static final String URI_AJOUTER_SALLE = "/salleafpa/creation";
	public static final String URI_RESERVER_SALLE = "/salleafpa/reserver";
	public static final String URI_RESERVER_UNE_SALLE  = "/salleafpa/reserver/salle";
	public static final String URI_VISUALISER_UNE_SALLE = "/salleafpa/visualiser/salle";
	
	public static final String URI_AJOUTER_BATIMENT = "/salleafpa/ajouter";
	public static final String URI_VISUALISER_RESERVATION = "/salleafpa/visualiser/reservation";
	
	public static final String URI_LISTE_RESERVATION = "/salleafpa/listeReservation";
	public static final String URI_SUPPRIMER_RESERVATION = "/salleafpa/deletereservation";
	
	
	
	
}
