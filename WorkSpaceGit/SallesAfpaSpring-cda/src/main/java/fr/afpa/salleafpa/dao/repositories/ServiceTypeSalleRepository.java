package fr.afpa.salleafpa.dao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.salleafpa.dao.entities.TypeSalleDao;

@Repository
public interface ServiceTypeSalleRepository extends JpaRepository<TypeSalleDao, Integer> {

}
