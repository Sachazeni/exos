package fr.afpa.salleafpa.metier.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.salleafpa.dto.iservices.IServiceReservationDto;
import fr.afpa.salleafpa.metier.entities.Reservation;
import fr.afpa.salleafpa.metier.iservices.IServiceReservation;

@Service
public class ServiceReservation implements IServiceReservation {

	@Autowired
	private IServiceReservationDto iServReservationDto;
	
	@Override
	public Reservation createReservation(Reservation reservation) {
		return iServReservationDto.createReservation(reservation);
	}

	@Override
	public Reservation getById(int id) {
		return iServReservationDto.getById( id);
	}

	@Override
	public Reservation update(Reservation reservation) {
		
		return iServReservationDto.update( reservation);
	}

	

	@Override
	public List<Reservation> getAll(int page, int nbReservPage) {
		return iServReservationDto.getAll(page,nbReservPage);
	}

	@Override
	public int nbPageListeReservation(int nbReservPage) {
		return iServReservationDto.nbPageListeReservation(nbReservPage);
	}

	@Override
	public void deleteById(int id) {
		 iServReservationDto.deleteById( id);
		
	}

}
