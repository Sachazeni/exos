package fr.afpa.salleafpa.controlleurs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.salleafpa.metier.entities.Personne;
import fr.afpa.salleafpa.metier.entities.TypeMateriel;
import fr.afpa.salleafpa.metier.iservices.IServicePersonne;
import fr.afpa.salleafpa.metier.iservices.IServiceTypeMateriel;

@RestController
public class webServiceControllers {
	
	@Autowired
	private IServicePersonne servPersonne;
	
	/**
	 * Methode pour recuperer la liste de personnes
	 * @return
	 */
	@GetMapping(value="/listerPersonnes")
	private List<Personne> recupPersonnes(){
	return servPersonne.getAllPersonnes();
		
	}
	
	
	
	
}
