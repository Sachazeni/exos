package fr.afpa.salleafpa.dto.services;

import java.util.ArrayList;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.salleafpa.dao.entities.MaterielDao;
import fr.afpa.salleafpa.dao.entities.TypeMaterielDao;
import fr.afpa.salleafpa.dao.repositories.TypeMaterielRepository;
import fr.afpa.salleafpa.dto.iservices.IServiceTypeMaterielDto;
import fr.afpa.salleafpa.metier.entities.TypeMateriel;
@Service
public class ServiceTypeMaterielDto implements IServiceTypeMaterielDto{
	@Autowired
	private TypeMaterielRepository typeMat;
	
	/**
	 * retourne une liste typeMaterielDAO
	 */
	public List<TypeMateriel> getAll() {
		List<TypeMaterielDao> listeTypeMaterielDao = typeMat.findAll();
		List<TypeMateriel> listeTypeMateriel = listeTypeMaterielDao.stream().map(ServiceTypeMaterielDto::typeMaterielDaoToTypeMateriel).collect(Collectors.toList());
		return listeTypeMateriel;
	}
	/**
	 * Methode pour transformer l'entite TypeMateriel DAO vers l'entite Type
	 * @param typeMaterielDao
	 * @return
	 */
	public static TypeMateriel typeMaterielDaoToTypeMateriel(TypeMaterielDao typeMaterielDao) {
		return new TypeMateriel(typeMaterielDao.getIdTypeMateriel(), typeMaterielDao.getLibelle());
	}
	/**
	 * Methode pour transformer l'entite TypeMateriel metier vers typeMateriel DAO
	 * @param typeMateriel
	 * @return
	 */
	
	public static TypeMaterielDao typeMaterielToTypeMaterielDao(TypeMateriel typeMateriel) {
		TypeMaterielDao typeMaterielDao =  new TypeMaterielDao();
		typeMaterielDao.setIdTypeMateriel(typeMateriel.getIdTypeMateriel());
		typeMaterielDao.setLibelle(typeMateriel.getLibelle());
		typeMaterielDao.setListeMAterielDao(new ArrayList<MaterielDao>());	
		return typeMaterielDao;
	}

	}

