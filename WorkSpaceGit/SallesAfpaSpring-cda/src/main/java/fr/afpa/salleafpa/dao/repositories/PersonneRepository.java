package fr.afpa.salleafpa.dao.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.afpa.salleafpa.dao.entities.PersonneDao;

@Repository
public interface PersonneRepository extends JpaRepository<PersonneDao, Integer> {

	@Query
	Optional<PersonneDao> findByLogin(String login);

}
