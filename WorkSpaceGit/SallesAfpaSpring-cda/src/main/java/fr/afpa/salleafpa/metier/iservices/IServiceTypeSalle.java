package fr.afpa.salleafpa.metier.iservices;

import java.util.List;

import fr.afpa.salleafpa.metier.entities.TypeSalle;


public interface IServiceTypeSalle {
	
	/**
	 * Methode pour retourner une liste de type de salles
	 * @return
	 */
	public List<TypeSalle> getAll();
	
	/**
	 * Methode pour retourner un seul type de salle via son id
	 * @param id : id pour recup le type de Salle
	 * @return : le type de salle trouvé
	 */
	public TypeSalle getOneTypeSalle(int id);
}
