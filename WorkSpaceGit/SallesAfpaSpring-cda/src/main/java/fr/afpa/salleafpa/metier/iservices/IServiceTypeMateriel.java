package fr.afpa.salleafpa.metier.iservices;

import java.util.List;

import fr.afpa.salleafpa.metier.entities.TypeMateriel;

public interface IServiceTypeMateriel{

	/**
	 * Methode pour retourner une liste de type de Materiel
	 * @return
	 */
	public List<TypeMateriel> getAll();
	
	public boolean saveTypeMateriel(String nom);
	public boolean modifTypeMateriel(String nom);

}

