package fr.afpa.salleafpa.dto.iservices;

import java.util.List;

import fr.afpa.salleafpa.metier.entities.TypeMateriel;

public interface IServiceTypeMaterielDto {
	
	/**
	 * Liste pour retourner une liste de type de materiel
	 * @return
	 */
	public List<TypeMateriel> getAll();
}
