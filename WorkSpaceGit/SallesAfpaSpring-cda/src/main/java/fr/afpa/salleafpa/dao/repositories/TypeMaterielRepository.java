package fr.afpa.salleafpa.dao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.salleafpa.dao.entities.TypeMaterielDao;
@Repository
public interface TypeMaterielRepository extends JpaRepository <TypeMaterielDao,Integer>{

	

}
