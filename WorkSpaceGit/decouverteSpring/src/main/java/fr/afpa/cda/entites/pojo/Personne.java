package fr.afpa.cda.entites.pojo;

public class Personne {
	private int id;
	private String nom;
	private Adresse adresse;

	public Personne(int id, String nom, Adresse adresse) {
		this.id = id;
		this.nom = nom;
		this.adresse=adresse;
	}
	public void afficher() {
		System.out.println(id+" "+nom+" "+adresse);
	}
}
