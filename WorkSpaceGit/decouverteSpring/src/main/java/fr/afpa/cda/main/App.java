package fr.afpa.cda.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.afpa.cda.entites.pojo.Personne;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
      ApplicationContext context =new ClassPathXmlApplicationContext("applicationContext.xml");
      Personne p = context.getBean("per", Personne.class);
      p.afficher();
    }
}
