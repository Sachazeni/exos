package fr.afpa.Gesper;



import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.entity.Adresse;
import fr.afpa.entity.Compte;
import fr.afpa.entity.Personne;
import fr.afpa.utils.HibernateUtils;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	//ouverture de la session
       Session connexion = HibernateUtils.getSession();
         
       //instanciation d'objet compte
       Compte compteTest =new Compte(1, "UserTest", "userMdp");
       
       
       //instanciation d'objet personne
       Personne persTest=new Personne(12, "Zimmer", "Marine");
       
       
       //creation de liens entre le compte et la personne ça va dans les deux sens
       persTest.setCompteLien(compteTest);
       compteTest.setPersonneLien(persTest);
       
       //creation liste des adresses
       List<Adresse>listeAdr=new ArrayList<Adresse>();
       
       //crea adresse
       Adresse adrTest = new Adresse(15, 1, "Rue Massena", "Lille");
       
       //ajout de l'adresse à la liste
       listeAdr.add(adrTest);
      
      //lien entre la personne et le compte dans les deux sens
      adrTest.setPersonne(persTest);
      persTest.setListeAdresses(listeAdr);
       
   // Début de la transaction
       Transaction tx = connexion.beginTransaction();
       
       
       connexion.save(compteTest);
       connexion.save(persTest);
       connexion.save(adrTest);
       
       tx.commit();
       connexion.close();
       
       
       
       
    		
    }
}
