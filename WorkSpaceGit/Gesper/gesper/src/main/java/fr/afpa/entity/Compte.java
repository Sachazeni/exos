package fr.afpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
@Entity
public class Compte {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id_compte;
	
	@Column
	private String login;
	
	@Column
	private String mdp;
	 
	//mapped by celui qui va donner sa cle primaire pour devenir cle etrangere dans la table Personne
	@OneToOne(mappedBy ="compteLien")
	private Personne personneLien;

	public Compte(int id_compte, String login, String mdp) {
		super();
		
		this.login = login;
		this.mdp = mdp;
	}
	
	
}
