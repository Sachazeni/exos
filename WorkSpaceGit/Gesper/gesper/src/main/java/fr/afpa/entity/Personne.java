package fr.afpa.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@Entity
public class Personne {
	
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="idPer")
private int idPer;
@Column(length=8)
private String nom;
@Column
private String prenom;

@OneToMany(mappedBy="personne")
private List<Adresse>listeAdresses;

//celui qui va recevoir la cle etrangere
@OneToOne
	@JoinColumn(name="personneLien")
private Compte compteLien;

public Personne(int idPer, String nom, String prenom) {
	super();
	this.idPer = idPer;
	this.nom = nom;
	this.prenom = prenom;
	listeAdresses =  new ArrayList<>();
}


}
