package fr.afpa.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
@Entity
public class Adresse {
	
@Id
@Column
private int idAd;

@Column
private int numero;

@Column
private String rue;

@Column
private String ville;

//cle etrangere se trouve du cote de l'adresse
@ManyToOne(cascade = {CascadeType.PERSIST})
@JoinColumn(name="idPer")
private Personne personne;

public Adresse(int idAd, int numero, String rue, String ville) {
	super();
	this.idAd = idAd;
	this.numero = numero;
	this.rue = rue;
	this.ville = ville;
}




}
