package fr.afpa.ihm;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import fr.afpa.controles.ControleSaisie;
import fr.afpa.entiteMetier.Adresse;
import fr.afpa.entiteMetier.Compte;
import fr.afpa.entiteMetier.UtilisateurEntite;
import fr.afpa.outils.Outils;
import fr.afpa.services.ServiceLien;

public class IhmMenu {
 
	
	public static void afficherMenu() {
		Outils.cls();
		System.out.println("            MENU\n");
		System.out.println("1 - Créer un utilisateur ");
		System.out.println("2 - Lister tous les Utilisateurs ");
		System.out.println("3 - Modifier un utilisateur ");
		System.out.println("4 - Supprimer un utilisateur\n ");
		System.out.println("5 - Quitter");
		
	}
	
	public void choixUtilisateur(Scanner scanner) {
		boolean quitter=false;
		String choix ="";
		while(!quitter) {
		afficherMenu();
		choix=scanner.nextLine();
		Outils.cls();
		switch(choix) {
		case "1": saisie(scanner);
			break;
		case "2":AffichagesSpecifiques.menuAfficherListe(scanner);
			break;
		case "3": AffichagesSpecifiques.choixUtilisateur(scanner,1);
			break;
		case "4": AffichagesSpecifiques.choixUtilisateur(scanner, 2);
			break; 
		case "5": quitter=true;
			System.out.println("Au revoir");
			break;
		default: System.out.println("Commande invalide!\n");
		}
		}
		
			
	}
	
	public void saisie(Scanner scanner) {
		ServiceLien servL=new ServiceLien();
		Adresse adr =null;
		Compte compte =null;
		UtilisateurEntite user = null;
		List<Adresse>listAdr=new ArrayList<Adresse>();
		
		String nom="";
		String prenom ="";
		String rue ="";
		String numero ="";
		String ville="";
		String login ="";
		String mdp="";
		//info utilisateur
		do {
		System.out.println("Entrez le nom de l'utilisateur (pas de chiffres) :");
		nom= scanner.nextLine().toUpperCase();
		}while (!ControleSaisie.controlSaisie(nom));
		do {
		System.out.println("Entrez le prenom de l'utilisateur (pas de chiffres) :");
		prenom= scanner.nextLine();
		}while (!ControleSaisie.controlSaisie(prenom));
		
		//info adresse
		do {
			System.out.println("Entrez le nom de la ville de l'utilisateur (pas de chiffres) :");	
			ville=scanner.nextLine();
		}while (!ControleSaisie.controlSaisie(ville));
		
		System.out.println("Entrez le nom de la rue de l'utilisateur :");	
		rue=scanner.nextLine();
		
		do {
			System.out.println("Entrez le numero de rue de l'utilisateur (doit commencer par un chiffre et peut comporter une lettre) :");	
			numero=scanner.nextLine();
		}while (!ControleSaisie.controlNumRue(numero));
		//init adresse
		adr =new Adresse(numero, rue, ville);
		//ajout de l'adresse dans la liste de l'utilisateur
		listAdr.add(adr);
		
		
		//infos compte
		System.out.println("Entrez le login pour l'utilisateur :");	
		login=scanner.nextLine();
		System.out.println("Entrez le mot de passe pour l'utilisateur :");	
		mdp=scanner.nextLine();
		//init Compte
		compte=new Compte(login, mdp);
		
		//init Utilisateur
		user=new UtilisateurEntite(nom, prenom, compte, listAdr);
		
		//appel service qui va faire le lien avec le dto
		servL.creationUtilisateur(user);
				
	}
	
	
}
