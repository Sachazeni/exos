package fr.afpa.ihm;

import java.util.List;
import java.util.Scanner;

import fr.afpa.controles.ControleSaisie;
import fr.afpa.entiteMetier.Adresse;
import fr.afpa.entiteMetier.UtilisateurEntite;
import fr.afpa.outils.Outils;
import fr.afpa.services.ServiceLien;

public class AffichagesSpecifiques {

	public static void afficherListe(List<UtilisateurEntite> listeUser, int affichage) {

		Outils.cls();
		if (listeUser.size() == 0) {
			System.out.println("Aucun utilisateur dans la base de données!");
		} else {
			for (UtilisateurEntite user : listeUser) {
				if (affichage == 2) {
					System.out.print(String.format("%-15s", user.getIdentifiant()));
				}
				System.out.print(String.format("%-15s", user.getNom()));
				System.out.print(String.format("%-10s", user.getPrenom()));
				System.out.print(String.format("%-10s", user.getListeAdresses()));
				System.out.println();

			}
		}

		System.out.println();

	}

	public static void menuAfficherListe(Scanner sc) {
		String choix = "";

		ServiceLien serv = new ServiceLien();

		List<UtilisateurEntite> listeUser = serv.listeUtilis();
		do {

			afficherListe(listeUser, 1);
			System.out.println("Pour revenir au menu precedent entrez Q");
			choix = sc.nextLine();

			if ("Q".equalsIgnoreCase(choix)) {

				return;
			} else {

			}
		} while (!"Q".equalsIgnoreCase(choix));
	}

	public static void choixUtilisateur(Scanner sc, int operation) {
		String choix = "";
		boolean quitter = false;

		ServiceLien serv = new ServiceLien();
		List<UtilisateurEntite> listeUser = serv.listeUtilis();

		do {

			afficherListe(listeUser, 2);

			System.out.println(
					"Veuillez entrer le numero de l'utilisateur recherché (des chiffres uniquement).\nPour revenir au menu precedent entrez Q ");

			choix = sc.nextLine();

			if ("Q".equalsIgnoreCase(choix)) {
				quitter = true;
				break;
			} else if (ControleSaisie.controlNum(choix)) {
				if (operation == 1) {
					afficherUserPourModification(sc, choix);
					quitter = true;
				}
				if (operation == 2) {
					serv.supressionUser(choix);
					quitter = true;
				}

			}

		} while (quitter == false);
	}

	private static void afficherUserPourModification(Scanner sc, String id) {
		ServiceLien serv = new ServiceLien();
		UtilisateurEntite user = serv.modificationUser(id);
		
		String choix = "";
		boolean quitter = false;
		if (user != null) {
			do {
				System.out.println("Veuillez faire un choix parmis les modifications disponibles pour l'utilisateur : "
						+ user.getNom() + " " + user.getPrenom());
				System.out.println("1- Modifier Nom");
				System.out.println("2- Modifier Prenom");
				System.out.println("3- Ajouter Adresse");
				System.out.println("\n4- Quitter");
				choix = sc.nextLine();
				Outils.cls();
				switch (choix) {
				case "1": demandeModif(sc, choix, user);
					break;
				case "2": demandeModif(sc, choix, user);
					break;
				case "3":demandeModif(sc, choix, user);
					break;
				case "4":
					quitter = true;
					break;
				default:
					break;

				}
			} while (quitter == false);
		} else {
			System.out.println("Utilisateur introuvable, q pour quitter.");
			choix = sc.nextLine();
		}
	}

	private static void demandeModif(Scanner sc, String choix, UtilisateurEntite user) {
		String nom = "";
		String prenom = "";
		String ville ="";
		String rue="";
		String numero="";
		Adresse adr =null;
		
		ServiceLien serv = new ServiceLien();
		switch (choix) {

		case "1":
			System.out.println("Veuillez entrer le nouveau nom de la personne : ");
			nom = sc.nextLine();
			user.setNom(nom);
			serv.userUpdate(user);
			break;

		case "2":
			System.out.println("Veuillez entrer le nouveau prenom de la personne : ");
			prenom = sc.nextLine();
			user.setPrenom(prenom);
			serv.userUpdate(user);
			break;
			
			
		case "3":
			System.out.println("Veuillez saisier une nouvelle adresse à ajouter pour la personne : ");
			
			do {
				System.out.println("Entrez le nom de la ville de l'utilisateur (pas de chiffres) :");	
				ville=sc.nextLine();
			}while (!ControleSaisie.controlSaisie(ville));
			
			System.out.println("Entrez le nom de la rue de l'utilisateur :");	
			rue=sc.nextLine();
			
			do {
				System.out.println("Entrez le numero de rue de l'utilisateur (doit commencer par un chiffre et peut comporter une lettre) :");	
				numero=sc.nextLine();
			}while (!ControleSaisie.controlNumRue(numero));
			//init adresse
			adr =new Adresse(numero, rue, ville);
			
			serv.userUpdateAdresse(adr, user);
			break;
		}
	}

}
