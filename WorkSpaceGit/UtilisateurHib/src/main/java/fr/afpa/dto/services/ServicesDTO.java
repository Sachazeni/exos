package fr.afpa.dto.services;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.dao.services.ServicesDao;
import fr.afpa.entiteMetier.Adresse;
import fr.afpa.entiteMetier.Compte;
import fr.afpa.entiteMetier.UtilisateurEntite;
import fr.afpa.entitePersistantes.AdresseDao;
import fr.afpa.entitePersistantes.CompteDao;
import fr.afpa.entitePersistantes.UtilisateurEntiteDao;

public class ServicesDTO {

	/**
	 * Methode pour transformer un utilisateur entite en objet de persisant puis le
	 * transmettre via la dao à la base de données.
	 * 
	 * @param user : utilisateur metier;
	 */
	public void dtoCreaUtilisateur(UtilisateurEntite user) {
		ServicesDao servDao = new ServicesDao();
		List<AdresseDao> listeAdrDao = new ArrayList<AdresseDao>();
		AdresseDao adrDao = null;
		CompteDao cmt = null;
		UtilisateurEntiteDao utilisateur = null;
		
		// init de l'instance persistante utilisateur
				utilisateur = new UtilisateurEntiteDao();
				utilisateur.setNom(user.getNom());
				utilisateur.setPrenom(user.getPrenom());
				
		// init de l'instance persistante Adresse
		for (Adresse adresse : user.getListeAdresses()) {
			adrDao = new AdresseDao(adresse.getNumero(), adresse.getRue(), adresse.getVille());
			adrDao.setUserLienA(utilisateur);
			listeAdrDao.add(adrDao);
		}

		// init de l'instance persistante compte
		cmt = new CompteDao();
		cmt.setLogin(user.getCompte().getLogin());
		cmt.setMdp(user.getCompte().getMdp());
		cmt.setUserLienC(utilisateur);
		// init de l'instance persistante utilisateur
		utilisateur.setListeAdresses(listeAdrDao); 
		utilisateur.setCompteLienC(cmt);
		servDao.enregistrerUtilisateur(utilisateur);

	}

	/**
	 * Transformation de la liste des utilisateursDao vers la liste
	 * Utilisateurs(metier)
	 * 
	 * @return
	 */
	public List<UtilisateurEntite> getListUsers() {
		ServicesDao servDao = new ServicesDao();
		UtilisateurEntite utilisateur = null;

		// Creation de la liste des utilisateurs(objet)
		List<UtilisateurEntite> listeUtilisateurs = new ArrayList<UtilisateurEntite>();

		// Creation de la liste des utilisateur(Persistants)
		List<UtilisateurEntiteDao> listeUtilisateursDaoRecup = servDao.recupListeUtilisateurs();

		// for each d'initialsation
		for (UtilisateurEntiteDao utilisateurEntiteDao : listeUtilisateursDaoRecup) {
			utilisateur = new UtilisateurEntite();
			utilisateur.setIdentifiant(utilisateurEntiteDao.getIdentifiant());
			List<Adresse> adresse = new ArrayList<Adresse>();

			for (AdresseDao adrDao : utilisateurEntiteDao.getListeAdresses()) {
				
				adresse.add(new Adresse(adrDao.getNumero(), adrDao.getRue(), adrDao.getVille()));

			}
			utilisateur.setListeAdresses(adresse);

			Compte cmpt = new Compte(utilisateurEntiteDao.getCompteLienC().getLogin(),
					utilisateurEntiteDao.getCompteLienC().getMdp());

			// init de l'utilisateur avec toutes les données necessaires.
			
			
			utilisateur.setNom(utilisateurEntiteDao.getNom());
			utilisateur.setPrenom(utilisateurEntiteDao.getPrenom());

			// init utilisateur(metier)
			utilisateur.setListeAdresses(adresse);
			utilisateur.setCompte(cmpt);

			// ajout de l'utilisateur dans la liste des utilisateurs.
			listeUtilisateurs.add(utilisateur);
		}

		return listeUtilisateurs;
	}
	
	/**
	 * Dto qui fait le lien pour supprimer l'utilisateur et de retourner si l'action a abouti
	 * @param id: identifiant entrée en parametres pour la personne a supprimer
	 * @return
	 */
	public boolean deleteUSer(int id) {
		
		ServicesDao servDao=new ServicesDao();
		return servDao.supressionUtilis(id);
	}
	
	public UtilisateurEntite getUtilisateurComplet(int id) {
		
		
		ServicesDao servDao=new ServicesDao();
		UtilisateurEntiteDao utilisateurD = servDao.recherherUtilisateur(id);
		Compte c =null;
		UtilisateurEntite user =null;
		List<Adresse>listAdr=new ArrayList<Adresse>();
		Adresse adr =null;
		
		if(utilisateurD!=null) {
			user =new UtilisateurEntite();
			c = new Compte(utilisateurD.getCompteLienC().getLogin(), utilisateurD.getCompteLienC().getMdp());
			
			user.setIdentifiant(utilisateurD.getIdentifiant());
			
			user.setNom(utilisateurD.getNom());
			user.setPrenom(utilisateurD.getPrenom());
			user.setCompte(c);
			for (AdresseDao adresseD : utilisateurD.getListeAdresses()) {
				adr=new Adresse();
				adr.setVille(adresseD.getVille());
				adr.setRue(adresseD.getRue());
				adr.setNumero(adresseD.getNumero());
				listAdr.add(adr);
				
			}
			user.setListeAdresses(listAdr);
		}
		else {
		
		
		}
		
		return user;
	}

	public void userUpdateDTo(UtilisateurEntite user) {
		ServicesDao servDao=new ServicesDao();
		UtilisateurEntiteDao utilisateurD = new UtilisateurEntiteDao();
		List<AdresseDao>listAdrD = new ArrayList<AdresseDao>();
		CompteDao cmptDao =new CompteDao();
		
		utilisateurD.setIdentifiant(user.getIdentifiant());
		utilisateurD.setNom(user.getNom());
		utilisateurD.setPrenom(user.getPrenom());

		
		cmptDao.setLogin(user.getCompte().getLogin());
		cmptDao.setMdp(user.getCompte().getMdp());
		cmptDao.setUserLienC(utilisateurD);
		utilisateurD.setCompteLienC(cmptDao);
		servDao.updateUserDao(utilisateurD);
		
	}

	public void userUpdateAdresse(UtilisateurEntite user, Adresse adresse) {
		AdresseDao adrD =null;
		ServicesDao servDao=new ServicesDao();
		
		UtilisateurEntiteDao utilisateurD = new UtilisateurEntiteDao();
		utilisateurD.setIdentifiant(user.getIdentifiant());
		
		adrD =new AdresseDao();
		adrD.setNumero(adresse.getNumero());
		adrD.setRue(adresse.getNumero());
		adrD.setVille(adresse.getVille());
		adrD.setUserLienA(utilisateurD);
		
		servDao.enregistrerAdresse(adrD);
		
	}
	
	
	public UtilisateurEntite getUser(String nom) {
		ServicesDao servDao=new ServicesDao();
		UtilisateurEntiteDao userDao =servDao.recupPersonne(nom);
		UtilisateurEntite user=null;
		if(userDao!=null) {
		user = new UtilisateurEntite(userDao.getNom());
		return user;
		
	}

}
