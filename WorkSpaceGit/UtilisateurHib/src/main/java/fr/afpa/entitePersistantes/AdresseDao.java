package fr.afpa.entitePersistantes;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import fr.afpa.dao.services.ServicesDao;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
@Getter
@Setter
@Entity
public class AdresseDao {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column
	private int idAd;

	@Column
	private String numero;

	@Column
	private String rue;

	@Column
	private String ville;

	//cle etrangere se trouve du cote de l'adresse
	@ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
	@JoinColumn(name="identifiant")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private UtilisateurEntiteDao userLienA;

	public AdresseDao (String numero, String rue, String ville) {
		super();
		
		this.numero = numero;
		this.rue = rue;
		this.ville = ville;
	}

	public AdresseDao() {
		super();
	}

	public AdresseDao(int idAd, String numero, String rue, String ville, UtilisateurEntiteDao userLienA) {
		super();
		this.idAd = idAd;
		this.numero = numero;
		this.rue = rue;
		this.ville = ville;
		this.userLienA = userLienA;
	}
}
