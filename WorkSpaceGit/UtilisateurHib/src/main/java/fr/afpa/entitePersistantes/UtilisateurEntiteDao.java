package fr.afpa.entitePersistantes;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.sun.istack.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
public class UtilisateurEntiteDao {
	
@Id	
@GeneratedValue(strategy = GenerationType.AUTO)
private int identifiant;

@Column
@NotNull
private String nom;

@Column
@NotNull
private String prenom;

@OneToMany(orphanRemoval = true, mappedBy="userLienA", fetch = FetchType.EAGER,cascade = CascadeType.ALL)
@OnDelete(action = OnDeleteAction.CASCADE)
private List<AdresseDao>listeAdresses;

//celui qui va recevoir la cle etrangere
@OneToOne(orphanRemoval = true,fetch = FetchType.EAGER,cascade = CascadeType.ALL)
@JoinColumn(name="userLienC")
@OnDelete(action = OnDeleteAction.CASCADE)
private CompteDao compteLienC;

public UtilisateurEntiteDao () {
	
}

public UtilisateurEntiteDao(String nom, String prenom, List<AdresseDao> listeAdresses, CompteDao compteLienC) {
	this.nom = nom;
	this.prenom = prenom;
	this.listeAdresses = listeAdresses;
	this.compteLienC = compteLienC;
}


 
}
