package fr.afpa.entitePersistantes;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.Getter;
import lombok.Setter;
	@Getter
	@Setter
	@Entity
public class CompteDao {

	
		@Id
		@GeneratedValue(strategy=GenerationType.AUTO)
		private int id_compte;
		
		@Column
		private String login;
		
		@Column
		private String mdp;
		 
		//mapped by celui qui va donner sa cle primaire pour devenir cle etrangere dans la table Personne
		@OneToOne(mappedBy ="compteLienC",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
		@OnDelete(action = OnDeleteAction.CASCADE)
		private UtilisateurEntiteDao userLienC;

		public CompteDao() {
			super();
		} 
		
		public CompteDao(String login, String mdp) {
			super();
			
			this.login = login;
			this.mdp = mdp;
		}

		public CompteDao(int id_compte, String login, String mdp, UtilisateurEntiteDao userLienC) {
			this.id_compte = id_compte;
			this.login = login;
			this.mdp = mdp;
			this.userLienC = userLienC;
		}

		
		
		
}
