package fr.afpa.dao.services;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import fr.afpa.entitePersistantes.AdresseDao;
import fr.afpa.entitePersistantes.UtilisateurEntiteDao;
import fr.afpa.utils.HibernateUtils;

public class ServicesDao {

	/**
	 * Methode pour recuperer la liste des utilisateurs
	 * 
	 * @return
	 */
	public List<UtilisateurEntiteDao> recupListeUtilisateurs() {

		Session connexion = HibernateUtils.getSession();

		Query q = connexion.createQuery("from UtilisateurEntiteDao");
		List<UtilisateurEntiteDao> listUtilisateurs = (List<UtilisateurEntiteDao>) q.list();
		connexion.close();
		return listUtilisateurs;

	}
	
	public UtilisateurEntiteDao recupPersonne(String nom) {
		
		Session connexion =null;
		UtilisateurEntiteDao persDao=null;
		try {
			connexion = HibernateUtils.getSession();
			
			//recherche par id
			Query q =connexion.createQuery("from Personne where nom ="+nom);
			
			persDao = (UtilisateurEntiteDao) q.getSingleResult();
			//recherche qui retourne plusieurs resultats
			List<UtilisateurEntiteDao> listUtilisateurs = (List<UtilisateurEntiteDao>) q.list();
		}catch (Exception e) {
			// TODO: handle exception
		}
		finally {
			if(connexion!=null) {
			connexion.close();
			}
			
		}
		return persDao;
		
	}

	/**
	 * Methode qui va enregistrer l'utilisateur persistant dans la base de données.
	 * 
	 * @param userDao : utilisateur transformée via le dto en une entité
	 *                persistante.
	 */
	public void enregistrerUtilisateur(UtilisateurEntiteDao userDao) {
		Session connexion = HibernateUtils.getSession();

		Transaction tx = connexion.beginTransaction();

		connexion.save(userDao);

		tx.commit();
		connexion.close();

	}

	public UtilisateurEntiteDao recherherUtilisateur(int identifiant) {

		Session connexion = HibernateUtils.getSession();

		UtilisateurEntiteDao utilisateurDao = null;

		utilisateurDao = (UtilisateurEntiteDao) connexion.get(UtilisateurEntiteDao.class, identifiant);
		connexion.close();
		return utilisateurDao;
	}

	public boolean supressionUtilis(int identifiant) {
		boolean flag = false;
		
		Session connexion = HibernateUtils.getSession();
		
		Transaction tx = connexion.beginTransaction();
		
		UtilisateurEntiteDao utilisateurDao = null;
		
		int id = 0;
		utilisateurDao = (UtilisateurEntiteDao) connexion.get(UtilisateurEntiteDao.class, identifiant);
		if (utilisateurDao != null) {
			id = utilisateurDao.getCompteLienC().getId_compte();
			Query q = connexion.createQuery("delete from CompteDao cmpt where cmpt.id_compte = " + id);
			flag = true;
			q.executeUpdate();
		}
		
			tx.commit();
			connexion.close();
			return flag;
		}
	
	public void updateUserDao(UtilisateurEntiteDao user) {
		Session connexion = HibernateUtils.getSession();

		Transaction tx = connexion.beginTransaction();
		connexion.update(user);
		tx.commit();
		connexion.close();
	}

	public void enregistrerAdresse(AdresseDao adrD) {
		Session connexion = HibernateUtils.getSession();

		Transaction tx = connexion.beginTransaction();
		connexion.update(adrD);
		tx.commit();
		connexion.close();
		
	}

	
}
