package fr.afpa.controles;

public class ControleSaisie {

	public static boolean  controlSaisie(String mot) {
		return (mot.matches("[a-zA-Z]*"));
	}
	public static boolean  controlNumRue(String mot) {
		return (mot.matches("\\d{1,}[a-zA-Z]?"));
	}
	public static boolean controlNum(String num) {
		return(num.matches("\\d+"));
	}
}
