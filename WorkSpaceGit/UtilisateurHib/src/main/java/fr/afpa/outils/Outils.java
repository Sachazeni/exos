package fr.afpa.outils;

public class Outils {
	
	/**
	 * Effacer la console
	 */
	public static void cls() {
		try {
			new ProcessBuilder("cmd","/c","cls").inheritIO().start().waitFor();
		}catch(Exception e) {
			System.out.println(e);
		}
	}
}
