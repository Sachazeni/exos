package fr.afpa.entiteMetier;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Adresse {
	
	private String numero;
	private String rue;
	private String ville;
	
	public Adresse(String numero, String rue, String ville) {
		super();
		this.numero = numero;
		this.rue = rue;
		this.ville = ville;
	}

	public Adresse() {
		super();
	}
	
	
}
