package fr.afpa.entiteMetier;


import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UtilisateurEntite {
	

//cote metier 
private int identifiant;
	
private String nom;

private String prenom;

private Compte compte;

private List<Adresse>listeAdresses;

public UtilisateurEntite () {
	
}

public UtilisateurEntite(String nom, String prenom, Compte compte, List<Adresse> list ) {
	this.nom = nom;
	this.prenom = prenom;
	this.compte =compte;
	this.listeAdresses = list;
}

public UtilisateurEntite(int identifiant, String nom, String prenom, Compte compte, List<Adresse> listeAdresses) {
	super();
	this.identifiant = identifiant;
	this.nom = nom;
	this.prenom = prenom;
	this.compte = compte;
	this.listeAdresses = listeAdresses;
}

 
}
