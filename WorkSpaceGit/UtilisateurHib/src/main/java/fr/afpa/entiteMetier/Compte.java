package fr.afpa.entiteMetier;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Compte {
	
	private String login;
	private String mdp;
	
	public Compte() {
		super();
	}

	
	public Compte(String login, String mdp) {
		super();
		this.login = login;
		this.mdp = mdp;
	}

	 
}
