package fr.afpa.services;

import java.util.List;

import fr.afpa.dto.services.ServicesDTO;
import fr.afpa.entiteMetier.Adresse;
import fr.afpa.entiteMetier.UtilisateurEntite;

public class ServiceLien {
	
	/**
	 * Methode qui fait le lient entre l'ihm et les services dto
	 * @param user : l'utilisateur qui va etre transforme en entité persistante dans le dto
	 */
	public void creationUtilisateur(UtilisateurEntite user) {
		ServicesDTO servDto=new ServicesDTO();
		servDto.dtoCreaUtilisateur(user);
		
	}
	
	/**
	 * Methode qui retourne la liste transforme de l'utilisateur persistant vers utilisateur entité
	 * @return : la liste de tous les utilisateurs
	 */
	public List<UtilisateurEntite> listeUtilis(){
		ServicesDTO servDto=new ServicesDTO();
		return servDto.getListUsers();
	}
	
	/**
	 * Methode qui va parser la chaine de caractere entrée en parametres et la transformer en int
	 * puis faire le lien avec ce nombre vers le service dto
	 * @param num
	 * @return
	 */
	public boolean supressionUser(String num) {
		int identifiant=Integer.parseInt(num);
		ServicesDTO servDto=new ServicesDTO();
		
		
			return servDto.deleteUSer(identifiant);
		
	}
	
	/**
	 * Methode qui va chercher et retourne l'utilisateur
	 * @param id l'id de l'utilisateur recherché
	 * @return l'utilisateur trouve null dans le cas contraire
	 */
	public UtilisateurEntite modificationUser(String id) {
	int ident = Integer.parseInt(id);
	ServicesDTO servDto=new ServicesDTO();
	
	return servDto.getUtilisateurComplet(ident);
	}
	/**
	 * Methode pour update un utiloisateur
	 * @param user
	 */
	public void userUpdate(UtilisateurEntite user) {
		ServicesDTO servDto=new ServicesDTO();
		servDto.userUpdateDTo(user);
		
	}
	
	/**
	 * Methode pour update l'adresse
	 * @param adresse
	 * @param user
	 */
	public void userUpdateAdresse(Adresse adresse, UtilisateurEntite user) {
		ServicesDTO servDto=new ServicesDTO();
		servDto.userUpdateAdresse(user, adresse);
		
	}
}
