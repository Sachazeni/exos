package fr.afpa.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.entities.Boutique;
import fr.afpa.entities.Produit;
import fr.afpa.repositories.AdresseRepository;
import fr.afpa.repositories.BoutiqueRepository;
import fr.afpa.repositories.ProduitRepository;

@RestController
public class ProduitController {

	@Autowired
	private ProduitRepository produitRepository;

	

	@Autowired
	private BoutiqueRepository boutiqueRepo;

	

//	/**
//	 * Methode-POST pour ajouter un produit dans la base de donn�es
//	 * 
//	 * @param produit : produit recu
//	 * @return le produit ( .save renvoit un produit)
//	 */
//	@PostMapping(value = "/produit")
//	public Produit addProduit(@RequestBody Produit produit) {
//
//		System.out.println(produit);
//
//		// init de la liste des adresses avec la liste des adresses du produit
//		List<AdresseProduit> listAdresseProd = produit.getAdresses();
//
//		// pour chaque adresse du produit
//		for (AdresseProduit adresse : listAdresseProd) {
//
//			// init AdresseProduit � null
//			AdresseProduit adr = null;
//
//			// Controle si l'id de l'adresse n'est pas null
//			if (adresse.getId() != null) {
//
//				// init de l'adresse cr�e via le repository adresseRepo avec l'id de l'adresse
//				// qui se trouve dans la liste
//				adr = adresseRepo.findById(adresse.getId()).orElse(null);
//
//				// reset avec les donn�e bdd de la liste des adresses du produit
//				listAdresseProd.set(listAdresseProd.indexOf(adresse), adr);
//			} else {
//				// si l'id null on sauvegarde la nouvelle adresse
//				adr = adresseRepo.save(adresse);
//			}
//		}
//		// on enregistre le produit
//		return produitRepository.saveAndFlush(produit);
//
//	}

	@DeleteMapping(value = "/delete/{id}")
	public void supprimerProduit(@PathVariable(value = "id") String id) {

		Long identifiant = Long.parseLong(id);
		// Suppression d'un produit en utilisant l'ID

		produitRepository.deleteById(identifiant);

	}

	@PutMapping(value = "/modifier")
	public Produit modifierProduit(@RequestBody Produit produit) {

		return produitRepository.save(produit);
	}
	
	@PostMapping(value = "/ajout")
	public Produit ajoutProduit(@RequestBody Produit produit) {
		produit.setBoutique(boutiqueRepo.findById(produit.getBoutique().getId()).orElse(null));
		return produitRepository.save(produit);
	}

	@GetMapping(value = "/listeProduits")
	@ResponseBody
	public ModelAndView showAll() {
		ArrayList<Produit> produits = (ArrayList<Produit>) produitRepository.findAll();
		ModelAndView mv = new ModelAndView();

		return mv;
	}
	
	//-----------------------------------------------------------------------------------------------------------------BOUTIQUE------------------*
	/**
	 * Methode-GET retourner toute la liste des produits trouv�s dans la base de
	 * donn�es
	 * 
	 * @return : liste de produits
	 */
	@GetMapping(value = "/produit")
	public List<Produit> getProduit() {
		return produitRepository.findAll();
	}
	
	@PostMapping (value="/newProduit")
	public Produit creerProduit(@RequestBody Produit produit) {
		
		return produitRepository.save(produit);
	}
	
	@PostMapping (value="/newBoutique")
	public Boutique creerBoutique(@RequestBody Boutique boutique) {
		
		return boutiqueRepo.save(boutique);
	}
	
	
	
	@PutMapping(value="/modifierBoutique")
	public Boutique modifierBoutique(@RequestBody Boutique boutique) {
		
		return boutiqueRepo.saveAndFlush(boutique);
	}
	
	@GetMapping(value="/listeboutique")
	public List<Boutique> listerBoutiques(){
		return boutiqueRepo.findAll();
	}

}
