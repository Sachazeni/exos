package fr.afpa.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
public class Boutique {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	long id;
	
	String nom;
	
	@OneToMany(cascade=CascadeType.MERGE, mappedBy="boutique", fetch=FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	List<Produit>produits = new ArrayList<Produit>();
	
	
 
}
