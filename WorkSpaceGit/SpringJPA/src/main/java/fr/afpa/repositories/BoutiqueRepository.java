package fr.afpa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.entities.Boutique;

@Repository
public interface BoutiqueRepository extends JpaRepository<Boutique, Long> {

}
