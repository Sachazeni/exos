package fr.afpa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.entities.Produit;

@Repository
public interface ProduitRepository extends JpaRepository<Produit, Long> {

	Produit findByLibelle(String libelle);

	Produit findByLibelleAndPrix(String libelle, Double prix);

}
