package fr.afpa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.entities.AdresseProduit;

@Repository
public interface AdresseRepository extends JpaRepository<AdresseProduit, Long> {

}
