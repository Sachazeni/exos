<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste des produits</title>
</head>
<body>
	<h1>Liste de produits</h1>
	<table id="table1">
		<tr>
			<th>Identifiant</th>
			<th>Libelle</th>
			<th> Prix </th>
		</tr>
		<c:forEach items="${ personnes }" var="personne">

			<tr>
				<th><a href="delete/${produit.id }" ><c:out value=" ${ produit.id } "></c:out></a></th>
				<th><c:out value=" ${ produit.libelle }" /></th>
				<th><c:out value=" ${ produit.prix }" /></th>
			</tr>


		</c:forEach>
		
		<br>
		<br>
		<a href="/ajouterPersonne" >Ajouter un produit</a>
</body>
</html>