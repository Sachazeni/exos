<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Result page</title>
</head>
<body>
	<h1>Liste de personnes</h1>
	<table id="table1">
		<tr>
			<th>Identifiant</th>
			<th>Nom</th>
			<th>Prénom</th>
		</tr>
		<c:forEach items="${ personnes }" var="personne">

			<tr>
				<th><a href="delete/${ personne.id }" ><c:out value=" ${ personne.id } "></c:out></a></th>
				<th><c:out value=" ${ personne.nom }" /></th>
				<th><c:out value=" ${ personne.prenom }" /></th>
			</tr>


		</c:forEach>
		
		<br>
		<br>
		<a href='<c:url value="/ajouterPersonne" />'> Ajouter une personne </a>
</body>
</html>