package fr.afpa.dao.repositories;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.afpa.dao.entities.Personne;

public interface PersonneRepository extends JpaRepository <Personne, Long> {

	Personne findByNomAndPrenom(String nom, String prenom);

	Personne findByNom(String nom);



}
