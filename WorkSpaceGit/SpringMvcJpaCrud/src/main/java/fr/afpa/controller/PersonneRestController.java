package fr.afpa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.dao.entities.Personne;
import fr.afpa.dao.repositories.PersonneRepository;

@RestController
public class PersonneRestController {

	@Autowired
	private PersonneRepository personneRepository;

	@GetMapping(value = "/listerPersonnes")
	@ResponseBody
	public List<Personne> showAll() {
		
		return personneRepository.findAll();
	}
	
	
	@GetMapping(value = "/personnesRest")
	@ResponseBody
	public List<Personne> personnes() {
		return personneRepository.findAll();
	}
	
	
	
	@GetMapping(value = "/personneRest/{id}")
	@ResponseBody
	public Personne afficherPersonne(@PathVariable(value = "id") int id) {
         // try catch si non 400
		Long idPersonne = (long) id;
	    return personneRepository.findById(idPersonne).orElse(new Personne());

	}
	
	
	@PostMapping(value = "/ajouterPersonneRest")
	@ResponseBody
	public Personne addPerson(@RequestBody Personne personne) {
		return personneRepository.save(personne);
	}
	
	
	@PutMapping(value = "/modifierPersonneRest")
	@ResponseBody
	public Personne ModifPersonne(@RequestBody Personne personne) {
		return personneRepository.saveAndFlush(personne);
	}
	
	
	
	
}
