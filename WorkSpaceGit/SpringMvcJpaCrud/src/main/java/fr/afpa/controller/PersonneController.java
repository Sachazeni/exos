package fr.afpa.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.dao.entities.Personne;
import fr.afpa.dao.repositories.PersonneRepository;

@Controller
public class PersonneController {

	@Autowired
	private PersonneRepository personneRepository;

/*	@GetMapping(value = "/ajouterPersonne")
	public String addPerson() {
		return "addPerson";
	}

	@PostMapping(value = "/ajouterPersonne")
	public ModelAndView addPerson(@RequestParam(value = "nom") String nom,
			@RequestParam(value = "prenom") String prenom) {
		Personne p1 = new Personne(nom, prenom);
		
		personneRepository.findByNomAndPrenom(nom, prenom);
		personneRepository.findByNom(nom);
		
		personneRepository.save(p1);
		ModelAndView mv = new ModelAndView();
		mv.setViewName("confirm");
		mv.addObject("nom", nom);
		mv.addObject("prenom", prenom);
		return mv;
	}

	@DeleteMapping(value = "/delete/{id}")
	public ModelAndView deletePersonne(@PathVariable(value = "id") int id) {
// try catch si non 400
		Long idPersonne = (long) id;
		// Suppression d'une personne en utilisant l'ID
		// Il faut verifier si la personne existe en utilisant l'id
		personneRepository.deleteById(idPersonne);
		

		// Recharger la liste de personnes
		return showAll();

	}

	@GetMapping(value = "/listerPersonnes")
	public ModelAndView showAll() {
		ArrayList<Personne> personnes = (ArrayList<Personne>) personneRepository.findAll();
		ModelAndView mv = new ModelAndView();
		mv.setViewName("result");
		mv.addObject("personnes", personnes);
		return mv;
	}
	
	
	@GetMapping(value = "/personnes")
	public String personnes() {
		return personneRepository.findAll().toString();
	}
	
	@GetMapping(value = "/personne/{id}")
	public String afficherPersonne(@PathVariable(value = "id") int id) {
         // try catch si non 400
		Long idPersonne = (long) id;
	    // Recharger la liste de personnes
		return personneRepository.findById(idPersonne).orElse(new Personne()).toString();

	}
	*/
	
}
