package fr.afpa.servelet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ControleFormulaireServ
 */
public class ControleFormulaireServ extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControleFormulaireServ() {
        super();
        // TODO Auto-generated constructor stub
    }


	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String teleph = request.getParameter("tel");
		String mail = request.getParameter("mail");
		String uri="";
		
		request.setAttribute("nom", nom);
		request.setAttribute("prenom", prenom);
		request.setAttribute("tel", teleph);
		request.setAttribute("mail", mail);
		
		
		if(!mail.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
			
			request.setAttribute("mailKo", "true");
			
			uri="formulaire.jsp";
			
			
	
		}
		if(!teleph.matches("[\\d]+")) {
			request.setAttribute("telKo", "true");
			
			uri="formulaire.jsp";
			
		
			
		}

		else if(mail.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")
				&&teleph.matches("[\\d]+")){
			
			uri="infos.jsp";
			
		}
	
		RequestDispatcher dispatcher = request.getRequestDispatcher(uri);
		dispatcher.forward(request, response);
	}


}
