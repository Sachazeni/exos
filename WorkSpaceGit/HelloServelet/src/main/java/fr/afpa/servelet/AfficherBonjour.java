package fr.afpa.servelet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AfficherBonjour
 */
public class AfficherBonjour extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AfficherBonjour() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append(
				"<h2> Formulaire </h2> "
				
				//important de mettre la methode 
				+ "<form action ='JEEurl' methode ='get'> "
				
				//mettre le nom car c'est via lui qu'on va recup les données
				+ "Nom:  <input type='text' name='nom'>"
				+ "<br>"
				+ "Prenom : <input type ='text' name= 'prenom'>"
				+ "<br>"
				+ "<input type ='submit' value ='Valider'>"
				+ "</form>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
