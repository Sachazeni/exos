import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//on importe App 
import App from './App';
import * as serviceWorker from './serviceWorker';

//rezct Dom autre bibliotheque du react qui vient avec react.
/**
 * La fonction render prend deux parametres, le compenent 
 */
ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
