import React from 'react';
//props = proprietes
/**
     * le return ne retourne qu'un seul element
     * la desciption sera affiche dans le paragraphe
     * input checkbox sera coche si props.completed est true
     * onchange = event qui s'appelera handleChange
     */
const TodoItem = props => {
    return (
        <div className="todo-item">
            <div className="description-wrapper">
                <p>{props.description}</p>
            </div>
            <div className="input-wrapper">
                <input type="checkbox"
                    defaultChecked={props.completed}
                    onChange={props.nomPropsPourEvent}
                />
            </div>
        </div>
    )
}

export default TodoItem;