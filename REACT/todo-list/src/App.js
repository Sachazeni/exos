import React, { Component } from 'react';
import './App.css';
import TodoItem from './ToDoItem/ToDoItem.js';
import todosData from './todosData.js';
class App extends Component{
  constructor() {
    super();
    this.state = {
      toDos: todosData
    }
  }
  event = id => {
    console.log('changement', id);
  }
  render() {
    const { toDos } = this.state;
    const tacheActive=toDos.filter(tache=>tache.completed===false);
    const tacheCompletes=toDos.filter(tache =>tache.completed===true);
    const finition =[...tacheActive,...tacheCompletes].map(item => {
      return (
        <TodoItem
          key={item.id}
          description={item.desciption}
          completed={item.completed}
          nomPropsPourEvent={() => {this.event(item.id) }}
        />
      )
    })
    return (
      <div className="App">
        {finition}
      </div>
    );

  }

}

export default App;
