import React, { useState } from 'react';

function PageLogin() {
    const [authentification, setAuthentification] = useState({});

    async function handleSubmitForm(event) {
        event.preventDefault();
        let response = await fetch(`${process.env.REACT_APP_API_URL}/auth`, {
            method: "POST",
            body: JSON.stringify(authentification),
            headers: {
                'Accept': 'application/json',
                "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then(response => { return response.json() })
            .catch(error => console.log(error));


        if (response) {
            console.log("oui")
            document.location.href = "/salle";
        } else {
            console.log("non")
        }
    }
    function changeHandler(event) {
        let authentificationTemp = authentification
        authentificationTemp[event.target.name]=event.target.value;
        setAuthentification(authentification)
       };

    return (
        <div className="container-fluid">
            <div className="row no-gutter">
                <div className="col-md-8 col-lg-6">
                    <div className="login d-flex align-items-center py-5">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-9 col-lg-8 mx-auto">
                                    <h3 className="login-heading mb-4">Connexion</h3>

                                    <form onSubmit={handleSubmitForm}>

                                        <div className="form-label-group">

                                            <input type="text" id="inputLogin"
                                             className="form-control" 
                                             placeholder="Votre Login" 
                                             name="login" 
                                             onChange={changeHandler}
                                             required autoFocus />
                                            <label htmlFor="inputLogin">Login</label>

                                        </div>

                                        <div className="form-label-group">

                                            <input type="password" 
                                            id="inputPassword" 
                                            className="form-control" 
                                            placeholder="Password" 
                                            name="mdp"
                                            onChange={changeHandler}
                                            required />
                                            <label htmlFor="inputPassword">Mot de Passe</label>

                                        </div>

                                        <div className="text-center">
                                            <a className="small" href="/inscription">S'inscrire</a>

                                        </div>
                                        <div className="col-md-9 col-lg-8 mx-auto">
                                    <button className="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2"
                                        type="submit">Connexion</button>
                                </div>
                                    </form>
                                </div>
                            </div>
                            <div className="row">
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div className="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div>

            </div>
        </div>

    );
}
export default PageLogin;