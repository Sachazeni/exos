import React from 'react';

function PageInscription() {
    return (
        <div className="container-fluid">
            <div className="row no-gutter">
                <div className="col-md-8 col-lg-6">
                    <div className="login d-flex align-items-center py-5">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-9 col-lg-8 mx-auto">
                                    <h3 className="login-heading mb-4">Inscription</h3>
                                    <form>
                                        <div className="form-label-group">
                                            <input type="text" id="inputNom" className="form-control" placeholder="Votre Nom" required autoFocus />
                                            <label htmlFor="inputNom">Nom</label>
                                        </div>

                                        <div className="form-label-group">
                                            <input type="text" id="inputPrenom" className="form-control" placeholder="Votre Prenom" required />
                                            <label htmlFor="inputPrenom">Prenom</label>
                                        </div>

                                        <div className="form-label-group">
                                            <input type="email" id="inputMail" className="form-control" placeholder="Votre mail" required />
                                            <label htmlFor="inputMail">Email</label>
                                        </div>

                                        <div className="form-label-group">
                                            <input type="text" id="inputFonction" className="form-control" placeholder="Votre fonction" required />
                                            <label htmlFor="inputMail">Fonction</label>
                                        </div>
                                        <button className="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Envoyer</button>

                                        <div className="text-center">
                                            <a className="small" href="/">Annuler</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div>

            </div>
        </div>

    );
}
export default PageInscription;