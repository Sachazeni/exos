import React from 'react';

import Login  from './Components/Login.js';
import Inscription  from './Components/Inscription.js';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Salle from './Components/Salle.js';

function App() {
  return (
    <div>
      <Router>
        <Switch>
          <Route path="/inscription" component ={Inscription}/>
          <Route path="/salle" component={Salle}/>
          <Route path="" component={Login}/>
        </Switch>
        {/**  <Switch>
  
        <Route path="">
        
        </Route>
        <Route path="/comments">
       //<Comments />
        </Route>
        <Route path="">
          <Users />
        </Route>
      </Switch>
       */ }
      </Router>
    </div>
  );
}

export default App;
