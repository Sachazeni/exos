import React from 'react';
import './App.css';
import Personne from './components/Personne.js';

function App() {
  return (
    <div className="App">
     <Personne nom='Elena'/>
     <Personne nom='Shini'/>
     <Personne nom='Cyanei'/>
    </div>
  );
}

export default App;
