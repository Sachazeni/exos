import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import './App.css';
import './index.css'
import 'bootstrap/dist/css/bootstrap.min.css';

import Users from './components/Users.js';
import Posts from './components/Posts.js';
import Comments from './components/Commentaires.js';

function App() {
  return (

    <div>
    <Router>
      {/* A <Switch> looks through its children <Route>s and
          renders the first one that matches the current URL. */}
      <Switch>
  
        <Route path="/posts">
         <Posts/>
        </Route>
        <Route path="/comments">
       <Comments />
        </Route>
        <Route path="">
          <Users />
        </Route>
      </Switch>
 
  </Router>
  </div>
  );
}


export default App;
