import React, { useState, useEffect } from 'react';

function Posts(){
    const [posts, setPosts] = useState([]);
    const userId=document.URL.split("?")[1];

    console.log(document.URL)
    const getPosts = () => {
        //regarder recup url
        console.log(`${process.env.REACT_APP_API_URL}/posts?${userId}`);
        
        fetch(`${process.env.REACT_APP_API_URL}/posts?${userId}`)

            .then(response => {
                return response.json();
            })

            .then(data => {
                setPosts(data);
            })
            .catch(error => {
                console.log(error);
            });
    }

    useEffect(() => {
        getPosts();}, []);
    const postList = posts.map((post) =>
    <tr key={post.id}>
        <td>{post.title}</td>
        <td>{post.body}</td>
        <td><a href = {`/comments?postId=${post.id}`}>Voir les commentaires</a></td>
        

    </tr>);

    return <div className="AppUser">
    <h4 className="enteteNomListes">Liste des posts de l'Utilisateur</h4>
    <p>Veuillez Selectionner un post pour voir les commentaires laissées :</p>
    <table className="table table-bordered">

      <thead>
        <tr>
          <th scope="col" >Titre</th>
          <th scope="col">Contenu</th>
          <th scope="col">Commentaires</th>
        </tr>
      </thead>
      <tbody>
      {postList}
      </tbody>
    </table>
  </div>;
}

export default Posts;

