import React, { useState, useEffect } from 'react';

function Users() {
  const [users, setUsers] = useState([]);

  const getUsers = () => {
    //utiliser les template string 
    fetch(`${process.env.REACT_APP_API_URL}/users`)
      .then(response => {
        return response.json();
      })
      .then(data => {
        setUsers(data);
      })
      .catch(error => {
        console.log(error);
      });
  }

  useEffect(() => {
    getUsers();
  }, []);
  const usersList = users.map((user) =>
    <tr key={user.id}>
      <td>{user.name}</td>
      <td>{user.username}</td>
      <td>{user.email}</td>
      <td><a href={`/posts?userId=${user.id}`} className="post">Voirs posts</a></td>
    </tr>);

  return <div className="AppUser">
    <h4 className="enteteNomListes">Liste des utilisateurs</h4>
    <p>Veuillez Selectionner un utilisateur pour voir les posts de celui ci :</p>
    <table className="table table-bordered">

      <thead>
        <tr>
          <th scope="col" >Nom</th>
          <th scope="col">Prenom</th>
          <th scope="col">Mail</th>
          <th scope="col">Posts</th>
        </tr>
      </thead>
      <tbody>
        {usersList}
      </tbody>
    </table>
  </div>;
}

export default Users;