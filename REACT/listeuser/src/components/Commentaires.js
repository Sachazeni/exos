import React, { useState, useEffect } from 'react';

function Commentaires() {

    const [coms, setComs] = useState([]);
    const postId = document.URL.split("?")[1];

    const getCommentaires = () => {
        fetch(`${process.env.REACT_APP_API_URL}/comments?` + postId)
            .then(response => {
                return response.json();
            })
            .then(data => {
                setComs(data);
            })
            .catch(error => {
                console.log(error);
            });
    }

    useEffect(() => {
        getCommentaires(); }, []);

    const comList = coms.map((commentaire) =>
    <tr key={commentaire.id}>
        <td>{commentaire.name}</td>
        <td>{commentaire.body}</td>
        <td>{commentaire.email}</td>
    </tr>);

   // return <ul className="list-group">{comList}</ul>;
    return <div className="AppUser">
    <h4 className="enteteNomListes">Liste des Commentaires du Post</h4>
    <table className="table table-bordered">
      <thead>
        <tr>
          <th scope="col" >Intitulé</th>
          <th scope="col">Contenu</th>
          <th scope="col">Mail de l'auteur</th>
        </tr>
      </thead>
      <tbody>
      {comList}
      </tbody>
    </table>
  </div>;
}

export default Commentaires;